﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Aspun.Infrastructure;

namespace Aspun.UnitTests.Infrastructure
{
    /// <summary>
    /// Summary description for LockManager_Tests
    /// </summary>
    [TestClass]
    public class LockManager_Tests
    {
        private const int CYCLES_COUNT = 100000;

        public LockManager_Tests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        //private LockManager lm;

        [TestInitialize()]
        public void MyTestInitialize()
        {
            //lm = new LockManager();
        }

        class SomeData
        {
            public int violationValue;
            public int violationTypeValue;

            public SomeData(int initialValue)
            {
                violationValue = initialValue;
                violationTypeValue = initialValue;
            }
        }

        class ThreadData
        {
            public SomeData data;
            public LockManager lm;
        }

        /// <summary>
        /// один owner изменяет объект
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            using (LockManager lm = new LockManager())
            {
                ThreadData threadData = new ThreadData();
                threadData.data = new SomeData(0);
                threadData.lm = lm;

                // Запустить несколько процессов паралельно. Они должны получать доступ к данным поочереди.
                int tasksCount = 50;
                Task[] tasks = new Task[tasksCount];

                for (int i = 0; i < tasksCount; i++)
                {
                    tasks[i] = new Task(DoSomething_Violation, threadData);
                }

                for (int i = 0; i < tasksCount; i++)
                {
                    tasks[i].Start();
                }
                Task.WaitAll(tasks);
                Assert.AreEqual(50 * CYCLES_COUNT, threadData.data.violationValue, "Процессы не разграничивают доступ к данным.");
            }
        }

        // два owner'а - один ресурс
        [TestMethod]
        public void TestMethod2()
        {
            using (LockManager lm = new LockManager())
            {
                ThreadData threadData = new ThreadData();
                threadData.data = new SomeData(0);
                threadData.lm = lm;

                // Запустить несколько процессов паралельно. Они должны получать доступ к данным поочереди.
                int tasksCount = 100;
                Task[] tasks = new Task[tasksCount];

                for (int i = 0; i < tasksCount; i++)
                {
                    if (i % 2 == 0)
                    {
                        tasks[i] = new Task(DoSomething_Violation, threadData);
                    }
                    else
                    {
                        tasks[i] = new Task(DoSomethingElse_Violation, threadData);
                    }
                }

                for (int i = 0; i < tasksCount; i++)
                {
                    tasks[i].Start();
                }
                Task.WaitAll(tasks);
                Assert.AreEqual(100 * CYCLES_COUNT, threadData.data.violationValue, "Процессы не разграничивают доступ к данным.");
            }
        }

        // 4 owner'а - 2 ресурса
        [TestMethod]
        public void TestMethod3()
        {
            using (LockManager lm = new LockManager())
            {
                ThreadData threadData = new ThreadData();
                threadData.data = new SomeData(0);
                threadData.lm = lm;

                // Запустить несколько процессов паралельно. Они должны получать доступ к данным поочереди.
                int tasksCount = 100;
                Task[] tasks = new Task[tasksCount];

                for (int i = 0; i < tasksCount / 2; i++)
                {
                    if (i % 2 == 0)
                    {
                        tasks[i] = new Task(DoSomething_Violation, threadData);
                        tasks[100 / 2 + i] = new Task(DoSomething_ViolationType, threadData);
                    }
                    else
                    {
                        tasks[i] = new Task(DoSomethingElse_Violation, threadData);
                        tasks[100 / 2 + i] = new Task(DoSomethingElse_ViolationType, threadData);
                    }
                }

                for (int i = 0; i < tasksCount; i++)
                {
                    tasks[i].Start();
                }
                Task.WaitAll(tasks);
                Assert.AreEqual(50 * CYCLES_COUNT, threadData.data.violationValue, "Процессы не разграничивают доступ к данным.");
                Assert.AreEqual(50 * CYCLES_COUNT, threadData.data.violationTypeValue, "Процессы не разграничивают доступ к данным.");
            }
        }

        private void DoSomething_Violation(object obj)
        {
            ThreadData data = (ThreadData)obj;

            data.lm.AcquireLock("DoSomething_Violation", LockResource.Violation);

            for (int i = 0; i < CYCLES_COUNT; i++)
            {
                data.data.violationValue += 1;
            }

            data.lm.ReleaseLock("DoSomething_Violation", LockResource.Violation);
        }

        private void DoSomethingElse_Violation(object obj)
        {
            ThreadData data = (ThreadData)obj;

            data.lm.AcquireLock("DoSomethingElse_Violation", LockResource.Violation);

            for (int i = 0; i < CYCLES_COUNT; i++)
            {
                data.data.violationValue += 1;
            }

            data.lm.ReleaseLock("DoSomethingElse_Violation", LockResource.Violation);
        }

        private void DoSomething_ViolationType(object obj)
        {
            ThreadData data = (ThreadData)obj;

            data.lm.AcquireLock("DoSomething_ViolationType", LockResource.ViolationType);

            for (int i = 0; i < CYCLES_COUNT; i++)
            {
                data.data.violationTypeValue += 1;
            }

            data.lm.ReleaseLock("DoSomething_ViolationType", LockResource.ViolationType);
        }

        private void DoSomethingElse_ViolationType(object obj)
        {
            ThreadData data = (ThreadData)obj;

            data.lm.AcquireLock("DoSomethingElse_ViolationType", LockResource.ViolationType);

            for (int i = 0; i < CYCLES_COUNT; i++)
            {
                data.data.violationTypeValue += 1;
            }

            data.lm.ReleaseLock("DoSomethingElse_ViolationType", LockResource.ViolationType);
        }
    }
}
