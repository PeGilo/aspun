﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Aspun.Web.Controllers
{
    public class ErrorController : ControllerBase
    {
        public ErrorController()
        {

        }
        //
        // GET: /Error/

        public ActionResult AccessDenied()
        {
            return View();
        }

        public ActionResult PageNotFound()
        {
            return View();
        }

    }
}
