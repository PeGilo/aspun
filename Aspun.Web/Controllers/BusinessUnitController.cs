﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aspun.Core.Contracts;
using Aspun.Core.Model;
using Aspun.Web.ViewModels;
using Aspun.Web.AppCode.Mvc;
using Aspun.Core.Services;

namespace Aspun.Web.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class BusinessUnitController : ControllerBase
    {
        private BusinessUnitService _service;
        private UserService _userService;

        public BusinessUnitController(IAspunUow uow)
            : base(uow)
        {
            _service = new BusinessUnitService(uow);
            _userService = new UserService(uow);
        }

        [CustomAuthorizeAttribute(RequiredBusinessUnitManagementPermission = Permission3.View)]
        public ActionResult Index()
        {
            IEnumerable<BusinessUnit> bunits = Uow.BusinessUnits.GetAllNotDeleted();

            BusinessUnitListEditModel listEditModel = new BusinessUnitListEditModel(bunits);

            return View(listEditModel);
        }


        [ActionName("Index")]
        [HttpPost]
        [CustomAuthorizeAttribute(RequiredBusinessUnitManagementPermission = Permission3.All)]
        [MultiButton(RegexFormKey = "^remove\\d+$")]
        public ActionResult Index_RemoveItem(BusinessUnitListEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("remove")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));
                model.Items.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("Index")]
        [HttpPost]
        [CustomAuthorizeAttribute(RequiredBusinessUnitManagementPermission = Permission3.All)]
        [MultiButton(MatchFormKey = "addItem")]
        public ActionResult Index_AddItem(BusinessUnitListEditModel model, FormCollection form)
        {
            var newItem = new BusinessUnitEditModel(new BusinessUnit());
            model.Items.Add(newItem);

            return View(model);
        }

        [ActionName("Index")]
        [HttpPost]
        [CustomAuthorizeAttribute(RequiredBusinessUnitManagementPermission = Permission3.All)]
        [MultiButton(MatchFormKey = "buttonSave")]
        public ActionResult Index_Save(BusinessUnitListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            List<BusinessUnit> newList = new List<BusinessUnit>();
            model.UpdateEntity(newList);

            _service.UpdateList(newList);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetNewBusinessUnitItem(int index)
        {
            var model = new BusinessUnitEditModel();
            ViewData["Index"] = index;

            return PartialView("EditorTemplates/BusinessUnitEditModel", model);
        }

        /// <summary>
        /// Переопределенный метод для загрузки в модели общих данных
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            // Дополнить модель правами пользователя
            if (filterContext.Controller.ViewData.Model is ViewModels.BusinessUnitListEditModel)
            {
                User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

                var model = (ViewModels.BusinessUnitListEditModel)filterContext.Controller.ViewData.Model;

                model.IsCreateEnabled = user.PermissionSet.BusinessUnitManagementPermission >= Permission3.All;
                model.IsSaveEnabled = user.PermissionSet.BusinessUnitManagementPermission >= Permission3.All;
                model.IsDeleteEnabled = user.PermissionSet.BusinessUnitManagementPermission >= Permission3.All;
            }
            else if (filterContext.Controller.ViewData.Model is ViewModels.BusinessUnitEditModel)
            {
                User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

                ViewData["isDeleteEnabled"] = user.PermissionSet.BusinessUnitManagementPermission >= Permission3.All;
                ViewData["isEditEnabled"] = user.PermissionSet.BusinessUnitManagementPermission >= Permission3.All;
            }
        }
    }
}
