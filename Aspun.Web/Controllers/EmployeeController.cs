﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aspun.Web.AppCode;
using Aspun.Web.AppCode.Excel;
using Aspun.Web.AppCode.Mvc;
using Aspun.Core.Contracts;
using Aspun.Core.Model;
using Aspun.Core.Services;
using Aspun.Web.ViewModels;
using Aspun.Common;

namespace Aspun.Web.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class EmployeeController : ControllerBase
    {
        private EmployeeService _employeeService;
        private UserService _userService;
        private BusinessUnitService _buService;

        public EmployeeController(IAspunUow uow)
            : base(uow)
        {
            _employeeService = new EmployeeService(uow);
            _userService = new UserService(uow);
            _buService = new BusinessUnitService(uow);
        }

        //
        // GET: /Employee/
        [CustomAuthorizeAttribute(RequiredEmployeeManagementPermission = Permission4Import.View)]
        public ActionResult Index(int? page, string sort, string sortdir /* ASC/DESC */)
        {
            // Список сотрудников необходимо отфильтровать по БЕ, к которым у пользователя есть доступ
            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

            // Извлекаем параметры запроса
            string sortField = sort;
            bool asc = "ASC".Equals(sortdir, StringComparison.InvariantCultureIgnoreCase) ? true : false;

            // Заполняем модель
            PagedEmployeesModel model = new PagedEmployeesModel();
            model.IsCreateEnabled = user.PermissionSet.EmployeeManagementPermission >= Permission4Import.InsertUpdateDelete;
            model.IsImportEnabled = user.PermissionSet.EmployeeManagementPermission >= Permission4Import.InsertUpdateDeleteImport;
            model.IsDeleteEnabled = user.PermissionSet.EmployeeManagementPermission >= Permission4Import.InsertUpdateDelete;
            model.IsEditEnabled = user.PermissionSet.EmployeeManagementPermission >= Permission4Import.InsertUpdateDelete;

            model.PageSize = 15;
            model.PageNumber = page.HasValue ? page.Value : 1;

            int totalRows = 0;
            IEnumerable<Employee> employees = GetEmployeesByUserAccess(user, model.PageSize, model.PageNumber, sortField, asc, out totalRows);

            //if (user.HasAccessToAllBusinessUnits)
            //{
            //    // Выбрать всех сотрудников
            //    employees = _employeeService.GetEmployees(model.PageSize, model.PageNumber, sortField, asc, out totalRows);
            //}
            //else
            //{
            //    if (user.BusinessUnits == null || user.BusinessUnits.Count == 0)
            //    {
            //        employees = new List<Employee>();
            //    }
            //    else
            //    {
            //        IEnumerable<string> buNames = user.BusinessUnits.Select(bu => bu.Name);
            //        employees = _employeeService.GetEmployees(buNames, model.PageSize, model.PageNumber, sortField, asc, out totalRows);
            //    }
            //}

            model.TotalRows = totalRows;
            model.Employees = employees;
            //foreach (User u in users)
            //{
            //    model.Employees.Add(new UserItemModel(u));
            //}

            return View(model);
        }

        [CustomAuthorizeAttribute(RequiredEmployeeManagementPermission = Permission4Import.InsertUpdateDeleteImport)]
        public ActionResult Import()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorizeAttribute(RequiredEmployeeManagementPermission = Permission4Import.InsertUpdateDeleteImport)]
        public ActionResult Import(string serverFileName)
        {
            ImplortEmployeesActionStatus result = new ImplortEmployeesActionStatus();

            try
            {
                FileManager fm = new FileManager(AppConfig);
                string fullFileName = fm.GetFileFullName(serverFileName);

                ExcelLoadParams excelParams = new ExcelLoadParams()
                {
                    StartRowIndex = AppConfig.AppSettings.ExcelStartRow,
                    StartColumnIndex = AppConfig.AppSettings.ExcelStartColumn,
                    NumberIndex = AppConfig.AppSettings.ExcelNumberIndex,
                    FIOIndex = AppConfig.AppSettings.ExcelFIOIndex,
                    PositionIndex = AppConfig.AppSettings.ExcelPositionIndex,
                    UnitIndex = AppConfig.AppSettings.ExcelUnitIndex,
                    BusinessUnitIndex = AppConfig.AppSettings.ExcelBusinessUnitIndex
                };
                ExcelReader excelReader = new ExcelReader(fullFileName, excelParams);

                EmployeeService empService = new EmployeeService(Uow);
                //result.CountEmployees = empService.ImportEmployees(excelReader.GetRecords());
                result.CountEmployees = empService.ImportEmployees(AppConfig.AppSettings.ConnectionString, excelReader.GetRecords());

                // Удалить файл после его использования
                fm.DeleteFile(serverFileName);
            }
            catch (Exception ex)
            {
                // TODO: Лог
                Logger.Error("Ошибка во время импорта сотрудников", ex);
                result.ErrorStatus = 1;
                result.ErrorMessage = ActionStatus.InnerError;
            }

            return PartialView("_ImportEmployeesResult", result);
        }


        //
        // GET: /Employee/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Employee/Create
        [CustomAuthorizeAttribute(RequiredEmployeeManagementPermission = Permission4Import.InsertUpdateDelete)]
        public ActionResult Create()
        {
            EmployeeCreateModel model = new EmployeeCreateModel();

            return View(model);
        }

        //
        // POST: /Employee/Create
        [HttpPost]
        [CustomAuthorizeAttribute(RequiredEmployeeManagementPermission = Permission4Import.InsertUpdateDelete)]
        public ActionResult Create(EmployeeCreateModel model, FormCollection collection)
        {
            ModelState.AddRuleViolations(model.GetRuleViolations(_employeeService));

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

            if (!UserHasAccessToBusinessUnit(user, model.BusinessUnit))
            {
                throw new InvalidOperationException("Пользователь не имеет доступа к указанной бизнес-единице.");
            }

            Employee employee = new Employee();

            model.UpdateEntity(employee);

            _employeeService.Create(employee);

            return RedirectToAction("Index");
        }

        //
        // GET: /Employee/Edit/5
        [CustomAuthorizeAttribute(RequiredEmployeeManagementPermission = Permission4Import.InsertUpdateDelete)]
        public ActionResult Edit(int id)
        {
            Employee employee = _employeeService.GetById(id);

            if (employee == null)
            {
                return NotFound();
            }

            EmployeeEditModel model = new EmployeeEditModel(employee);
            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

            if (!UserHasAccessToBusinessUnit(user, model.BusinessUnit))
            {
                throw new InvalidOperationException("Пользователь не имеет доступа к указанной бизнес-единице.");
            }

            return View(model);
        }

        //
        // POST: /Employee/Edit/5

        [HttpPost]
        [CustomAuthorizeAttribute(RequiredEmployeeManagementPermission = Permission4Import.InsertUpdateDelete)]
        public ActionResult Edit(EmployeeEditModel model, int id, FormCollection collection)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

            if (!UserHasAccessToBusinessUnit(user, model.BusinessUnit))
            {
                throw new InvalidOperationException("Пользователь не имеет доступа к указанной бизнес-единице.");
            }

            Employee employee = _employeeService.GetById(id);

            if (employee == null)
            {
                return NotFound();
            }

            model.UpdateEntity(employee);

            _employeeService.Update(employee);

            return RedirectToAction("Index");
        }

        //
        // GET: /Employee/Delete/5
        [CustomAuthorizeAttribute(RequiredEmployeeManagementPermission = Permission4Import.InsertUpdateDelete)]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Employee/Delete/5
        [HttpPost]
        [CustomAuthorizeAttribute(RequiredEmployeeManagementPermission = Permission4Import.InsertUpdateDelete)]
        public ActionResult Delete(int id, FormCollection collection)
        {
            _employeeService.DeleteEmployee(id);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Переопределенный метод для загрузки в модели общих данных
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            // Дополнить модель списком бизнес-единиц
            if (filterContext.Controller.ViewData.Model is ViewModels.EmployeeCreateModel)
            {
                var model = (ViewModels.EmployeeCreateModel)filterContext.Controller.ViewData.Model;
                model.BusinessUnitList = GetBusinessUnitList();
            }
        }

    }
}
