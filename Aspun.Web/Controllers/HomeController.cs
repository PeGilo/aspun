﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aspun.Core.Contracts;
using Aspun.Web.AppCode.Mvc;

namespace Aspun.Web.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class HomeController : ControllerBase
    {
        public HomeController(IAspunUow uow)
            : base(uow)
        {

        }

        [CustomAuthorizeAttribute()]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Violation");
        }
    }
}
