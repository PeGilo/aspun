﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aspun.Core.Contracts;
using Aspun.Core.Model;
using Aspun.Web.ViewModels;
using Aspun.Web.AppCode.Mvc;
using Aspun.Core.Services;

namespace Aspun.Web.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ViolationTypeController : ControllerBase
    {
        private ViolationTypeService _service;
        private UserService _userService;

        public ViolationTypeController(IAspunUow uow)
            : base(uow)
        {
            _service = new ViolationTypeService(uow);
            _userService = new UserService(uow);
        }

        //
        // GET: /ViolationType/
        [CustomAuthorizeAttribute(RequiredViolationTypeManagementPermission = Permission3.View)]
        public ActionResult Index()
        {
            IEnumerable<ViolationType> vtypes = Uow.ViolationTypes.GetAll();

            ViolationTypeListEditModel listEditModel = new ViolationTypeListEditModel(vtypes);

            return View(listEditModel);
        }


        [ActionName("Index")]
        [HttpPost]
        [MultiButton(RegexFormKey = "^remove\\d+$")]
        [CustomAuthorizeAttribute(RequiredViolationTypeManagementPermission = Permission3.All)]
        public ActionResult Index_RemoveItem(ViolationTypeListEditModel model, FormCollection form)
        {
            string res = (from item in form.AllKeys
                          where item.StartsWith("remove")
                          select item).FirstOrDefault();

            if (res != null)
            {
                // Post command to remove an item
                int index = Int32.Parse(res.Substring(6));
                model.Items.RemoveAt(index);

                // Очистить ModelState иначе список возьмется из него
                ModelState.Clear();
            }

            return View(model);
        }

        [ActionName("Index")]
        [HttpPost]
        [MultiButton(MatchFormKey = "addItem")]
        [CustomAuthorizeAttribute(RequiredViolationTypeManagementPermission = Permission3.All)]
        public ActionResult Index_AddItem(ViolationTypeListEditModel model, FormCollection form)
        {
            var newItem = new ViolationTypeEditModel(new ViolationType());
            model.Items.Add(newItem);

            return View(model);
        }

        [ActionName("Index")]
        [HttpPost]
        [MultiButton(MatchFormKey = "buttonSave")]
        [CustomAuthorizeAttribute(RequiredViolationTypeManagementPermission = Permission3.All)]
        public ActionResult Index_Save(ViolationTypeListEditModel model, FormCollection form)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            List<ViolationType> newList = new List<ViolationType>();
            model.UpdateEntity(newList);

            _service.UpdateList(newList);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetNewViolationTypeItem(int index)
        {
            var model = new ViolationTypeEditModel();
            ViewData["Index"] = index;

            return PartialView("EditorTemplates/ViolationTypeEditModel", model);
        }

        /// <summary>
        /// Переопределенный метод для загрузки в модели общих данных
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            // Дополнить модель правами пользователя
            if (filterContext.Controller.ViewData.Model is ViewModels.ViolationTypeListEditModel)
            {
                User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

                var model = (ViewModels.ViolationTypeListEditModel)filterContext.Controller.ViewData.Model;

                model.IsCreateEnabled = user.PermissionSet.ViolationTypeManagementPermission >= Permission3.All;
                model.IsSaveEnabled = user.PermissionSet.ViolationTypeManagementPermission >= Permission3.All;
                model.IsDeleteEnabled = user.PermissionSet.ViolationTypeManagementPermission >= Permission3.All;
            }
            else if (filterContext.Controller.ViewData.Model is ViewModels.ViolationTypeEditModel)
            {
                User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

                ViewData["isDeleteEnabled"] = user.PermissionSet.ViolationTypeManagementPermission >= Permission3.All;
                ViewData["isEditEnabled"] = user.PermissionSet.ViolationTypeManagementPermission >= Permission3.All;
            }
        }
    }
}
