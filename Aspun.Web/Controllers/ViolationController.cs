﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aspun.Web.AppCode;
using Aspun.Web.AppCode.Mvc;
using Aspun.Core.Contracts;
using Aspun.Core.Model;
using Aspun.Core.Services;
using Aspun.Web.ViewModels;
using Aspun.Common;

namespace Aspun.Web.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ViolationController : ControllerBase
    {
        private UserService _userService;
        private ViolationService _violationService;
        private ViolationTypeService _violationTypeService;
        private EmployeeService _employeeService;
        private BusinessUnitService _businesUnitService;

        private const int MAX_SEARCH_COUNT = 15;
        private readonly string[] SEARCH_FIELDS = new string[] { "FIO", "Number", "Position" };

        public ViolationController(IAspunUow uow)
            : base(uow)
        {
            _userService = new UserService(uow);
            _violationService = new ViolationService(uow);
            _violationTypeService = new ViolationTypeService(uow);
            _employeeService = new EmployeeService(uow);
            _businesUnitService = new BusinessUnitService(uow);
        }

        [CustomAuthorizeAttribute(RequiredViolationManagementPermission = Permission4.View)]
        public ActionResult Index(int? page, string sort, string sortdir /* ASC/DESC */)
        {
            // Список сотрудников необходимо отфильтровать по БЕ, к которым у пользователя есть доступ
            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

            // Извлекаем параметры запроса
            string sortField = sort;
            bool asc = "ASC".Equals(sortdir, StringComparison.InvariantCultureIgnoreCase) ? true : false;

            // Заполняем модель
            PagedViolationsModel model = new PagedViolationsModel();
            model.UserCanEdit = user.PermissionSet.ViolationManagementPermission >= Permission4.InsertUpdate;
            model.IsCreateEnabled = user.PermissionSet.ViolationManagementPermission >= Permission4.InsertUpdate;

            model.PageSize = 15;
            model.PageNumber = page.HasValue ? page.Value : 1;
            
            int totalRows = 0;
            IEnumerable<Violation> violations;

            if (user.HasAccessToAllBusinessUnits)
            {
                // Выбрать всех сотрудников
                violations = _violationService.GetViolations(model.PageSize, model.PageNumber, sortField, asc, out totalRows);
            }
            else
            {
                if (user.BusinessUnits == null || user.BusinessUnits.Count == 0)
                {
                    violations = new List<Violation>();
                }
                else
                {
                    IEnumerable<string> buNames = user.BusinessUnits.Select(bu => bu.Name);
                    violations = _violationService.GetViolations(buNames, model.PageSize, model.PageNumber, sortField, asc, out totalRows);
                }
            }

            model.TotalRows = totalRows;
            //model.Violations = violations;
            foreach (Violation v in violations)
            {
                model.Violations.Add(new ViolationBriefModel(v));
            }

            return View(model);
        }

        [CustomAuthorizeAttribute(RequiredViolationManagementPermission = Permission4.InsertUpdate)]
        public ActionResult Create()
        {
            ViolationCreateModel model = new ViolationCreateModel();

            // Initialize default values
            model.DateString = DateTime.Now.ToShortDateString();

            return View(model);
        }

        [HttpPost]
        [CustomAuthorizeAttribute(RequiredViolationManagementPermission = Permission4.InsertUpdate)]
        public ActionResult Create(ViolationCreateModel model, FormCollection collection)
        {
            ModelState.AddRuleViolations(model.GetRuleViolations(_violationService));

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // Загрузить по ViolationId описание нарушения и параграф нарушения
            ViolationType selectedViolationType = _violationTypeService.GetById(model.SelectedViolationId);
            if (selectedViolationType == null)
            {
                ModelState.AddModelError("SelectedViolationId", "Выбран недействительный тип нарушения.");
                return View(model);
            }

            BusinessUnit selectedBusinessUnit = _businesUnitService.GetById(model.SelectedBusinessUnitId);
            if (selectedBusinessUnit == null)
            {
                ModelState.AddModelError("SelectedBusinessUnitId", "Выбрано недействительное предприятие.");
                return View(model);
            }

            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

            //// Загрузить по EmployeeId аттрибуты сотрудника
            //Employee employee = model.SelectedEmployeeId.HasValue ? _employeeService.GetById(model.SelectedEmployeeId.Value) : null;
            //if (employee == null)
            //{
            //    ModelState.AddModelError("SelectedEmployeeId", "Не выбран сотрудник.");
            //    return View(model);
            //}

            //// Проверить, что совпадает выбранные Business Unit и Business Unit 
            //if (!String.Equals(employee.BusinessUnit, model.BusinessUnitName, StringComparison.InvariantCultureIgnoreCase))
            //{
            //    ModelState.AddModelError("SelectedEmployeeId", "Бизнес еденица в которой состоит сотрудник не совпадает с выбранной.");
            //    return View(model);
            //}

            // Проверить, что пользователь имеет доступ к Buinsees Unit
            if (!UserHasAccessToBusinessUnit(user, selectedBusinessUnit.Name /*model.BusinessUnitName*/))
            {
                throw new InvalidOperationException("Пользователь не имеет доступа к указанной бизнес-единице.");
            }

            Violation violation = new Violation();

            model.UpdateEntity(violation);

            // Заполнить дополнительные поля, которые не входят в модель
            violation.CreatorUserName = HttpContext.User.Identity.Name;
            violation.ViolationTypeDescription = selectedViolationType.Description;
            violation.ViolationTypePar = selectedViolationType.RegulationsPar;
            violation.ViolationTypeCode = selectedViolationType.Code;
            violation.BusinessUnitName = selectedBusinessUnit.Name;
            violation.Prefix = selectedBusinessUnit.Prefix;

            _violationService.Create(violation);
            
            // Метод Create обновил индекс (номер) нарушения так что его можно использовать


            //return RedirectToAction("Index");

            return View("Created", violation);
        }

        //[CustomAuthorizeAttribute(RequiredViolationManagementPermission = Permission4.InsertUpdate)]
        //public ActionResult Created()
        //{
        //}

        [CustomAuthorizeAttribute(RequiredViolationManagementPermission = Permission4.InsertUpdate)]
        public ActionResult Edit(int id)
        {
            Violation violation = _violationService.GetById(id);

            if (violation == null)
            {
                return NotFound();
            }
            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

            ViolationEditModel model = new ViolationEditModel(violation);
            model.IsDeleteEnabled = (user.PermissionSet.ViolationManagementPermission >= Permission4.All);

            return View(model);
        }

        [HttpPost]
        [CustomAuthorizeAttribute(RequiredViolationManagementPermission = Permission4.InsertUpdate)]
        public ActionResult Edit(ViolationEditModel model, int id, FormCollection collection)
        {
            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);
            model.IsDeleteEnabled = (user.PermissionSet.ViolationManagementPermission >= Permission4.All);

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // Проверить что пользователь может редактировать нарушения из указанной БЕ

            if (!UserHasAccessToBusinessUnit(user, model.BusinessUnitName))
            {
                throw new InvalidOperationException("Пользователь не имеет доступа к указанной бизнес-единице.");
            }

            Violation violation = _violationService.GetById(id);

            if (violation == null)
            {
                return NotFound();
            }

            model.UpdateEntity(violation);

            _violationService.Update(violation);

            return RedirectToAction("Index");
        }

        [CustomAuthorizeAttribute(RequiredViolationManagementPermission = Permission4.View)]
        public ActionResult Details(int id)
        {
            Violation violation = _violationService.GetById(id);

            if (violation == null)
            {
                return NotFound();
            }

            // Проверить что пользователь имеет доступ к этой БЕ
            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

            if (!UserHasAccessToBusinessUnit(user, violation.BusinessUnitName))
            {
                throw new InvalidOperationException("Пользователь не имеет доступа к указанной бизнес-единице.");
            }

            ViolationDetailsModel model = new ViolationDetailsModel(violation);

            return View(model);
        }

        [CustomAuthorizeAttribute(RequiredViolationManagementPermission = Permission4.All)]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        [HttpPost]
        [CustomAuthorizeAttribute(RequiredViolationManagementPermission = Permission4.All)]
        public ActionResult Delete(int id, FormCollection collection)
        {
            _violationService.DeleteViolation(id);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Для autocomplete по сотрудникам
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        [CustomAuthorizeAttribute(RequiredViolationManagementPermission = Permission4.InsertUpdate)]
        public ActionResult Employees(string term)
        {
            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);
            if (user != null)
            {
                var elist = (from e in SearchEmployees(user, term)
                             select new { 
                                id = e.EmployeeId, 
                                value = e.Number +", " + e.FIO + " (" + e.Position + ", " + e.BusinessUnit + ")",
                                fio = e.FIO,
                                number = e.Number,
                                position= e.Position,
                                unit = e.Unit,
                                bu = e.BusinessUnit
                             }).ToList();
                return Json(elist, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var list = new[] { 
                        new { id = "", value = "" }
                    }.ToList();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Переопределенный метод для загрузки в модели общих данных
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            // Дополнить модель списком бизнес-единиц

            if (filterContext.Controller.ViewData.Model is ViewModels.ViolationBaseModel)
            {
            }

            if (filterContext.Controller.ViewData.Model is ViewModels.ViolationCreateModel)
            {
                var model = (ViewModels.ViolationCreateModel)filterContext.Controller.ViewData.Model;
                model.BusinessUnitList = GetBusinessUnitListWithId();
                model.ViolationList = GetViolationsList();
            }
            else if (filterContext.Controller.ViewData.Model is ViewModels.ViolationEditModel)
            {
            }
        }

        protected virtual SelectList GetViolationsList()
        {
            IEnumerable<ViolationType> vtypes = _violationTypeService.GetAll();

            //IList<ListItem> list = _services.SettingsService.GetResourceSupplyTypes();
            //list.Insert(0, new ListItem(0, ""));

            //return new SelectList(list, "Id", "Text");

            return new SelectList(
                (from vtype in vtypes
                 select
                     new
                     {
                         Id = vtype.ViolationTypeId,
                         Text = vtype.Code + " " + vtype.Description 
                                + (String.IsNullOrEmpty(vtype.RegulationsPar) ? "" : " (" + vtype.RegulationsPar + ")")
                     }
                 ), "Id", "Text");
        }

        protected virtual IEnumerable<Employee> SearchEmployees(User user, string term)
        {
            IEnumerable<Employee> employees;

            employees = SearchEmployeesByUserAccess(user, term);

            return employees;
        }

        protected virtual IEnumerable<Employee> SearchEmployeesByUserAccess(User user, string term)
        {
            IEnumerable<Employee> employees;

            if (user.HasAccessToAllBusinessUnits)
            {
                // Выбрать всех сотрудников
                employees = _employeeService.SearchEmployees(MAX_SEARCH_COUNT, SEARCH_FIELDS, term);
            }
            else
            {
                if (user.BusinessUnits == null || user.BusinessUnits.Count == 0)
                {
                    employees = new List<Employee>();
                }
                else
                {
                    IEnumerable<string> buNames = user.BusinessUnits.Select(bu => bu.Name);
                    employees = _employeeService.SearchEmployees(buNames, MAX_SEARCH_COUNT, SEARCH_FIELDS, term);
                }
            }

            return employees;
        }

    }
}