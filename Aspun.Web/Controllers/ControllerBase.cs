﻿using Aspun.Core.Contracts;
using Aspun.Web.AppCode.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aspun.Web.AppCode;
using Aspun.Core.Model;
using Aspun.Core.Services;

namespace Aspun.Web.Controllers
{
    public class ControllerBase : Controller
    {
        /// <summary>
        /// м.б. null если не вызван конструктор с параметрами
        /// </summary>
        private IAspunUow _uow;

        /// <summary>
        /// м.б. null если не вызван конструктор с параметрами
        /// </summary>
        protected IAspunUow Uow { get { return _uow; } }

        private AppConfiguration _appConfig;

        /// <summary>
        /// м.б. null если не вызван конструктор с параметрами
        /// </summary>
        private UserService _userService;

        /// <summary>
        /// м.б. null если не вызван конструктор с параметрами
        /// </summary>
        private BusinessUnitService _buService;

        /// <summary>
        /// м.б. null если не вызван конструктор с параметрами
        /// </summary>
        private EmployeeService _employeeService;


        protected virtual AppConfiguration AppConfig
        {
            get { return _appConfig; }
        }

        public ControllerBase()
        {
            _appConfig = new AppConfiguration();
        }

        public ControllerBase(IAspunUow uow)
        {
            _uow = uow;

            _appConfig = new AppConfiguration();

            _userService = new UserService(uow);
            _buService = new BusinessUnitService(uow);
            _employeeService = new EmployeeService(uow);
        }

        /// <summary>
        /// Выполняет необходимые действия при возникновении ситуации отсутствия 
        /// запрашиваемых данных в базе
        /// </summary>
        /// <returns></returns>
        protected virtual ActionResult NotFound()
        {
            return RedirectToAction("PageNotFound", "Error");
        }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            // Залогировать
            Logger.Error("Global Exception", filterContext.Exception);

            // Не использовать редирект, а подменять View, чтобы не вызывать еще один exception

            ViewDataDictionary viewData = new ViewDataDictionary();

            string viewName = "Error"; // default view

            if (filterContext.Exception is InvalidOperationException)
            {
                viewData["ErrorMessage"] = filterContext.Exception.Message;
            }

            filterContext.ExceptionHandled = true;

            filterContext.Result = new ViewResult
            {
                ViewName = viewName,
                ViewData = viewData
            };
        }


        protected virtual bool UserHasAccessToBusinessUnit(User user, string buName)
        {
            if (user.HasAccessToAllBusinessUnits)
            {
                return true;
            }
            else if (user.BusinessUnits != null)
            {
                foreach (var bu in user.BusinessUnits)
                {
                    if (String.Equals(bu.Name, buName, StringComparison.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Выбирает БЕ доступные текущему пользователю
        /// </summary>
        /// <param name="hasEmpty">Добавлять ли пустой item в список</param>
        /// <returns></returns>
        protected virtual SelectList GetBusinessUnitList(bool hasEmpty = false)
        {
            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

            IList<SelectListItem> list;

            if (user.HasAccessToAllBusinessUnits)
            {
                list = (from bu in _buService.GetAllNotDeleted() select bu).ToList()
                        .Select(bu => new SelectListItem() { Value = bu.Name, Text = bu.Name }).ToList();
            }
            else if (user.BusinessUnits != null)
            {
                list = (from bu in user.BusinessUnits where bu.IsDeleted == false select bu).ToList()
                        .Select(bu => new SelectListItem() { Value = bu.Name, Text = bu.Name }).ToList();
            }
            else
            {
                list = new List<SelectListItem>();
            }

            if (hasEmpty)
            {
                list.Insert(0, new SelectListItem() { Text = "", Value = "" });
            }
            //IList<ListItem> list = _services.SettingsService.GetResourceSupplyTypes();
            //

            return new SelectList(list, "Value", "Text");
        }

        // TODO: Объединить с методом GetBusinessUnitList
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hasEmpty"></param>
        /// <returns></returns>
        protected virtual SelectList GetBusinessUnitListWithId(bool hasEmpty = false)
        {
            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

            IList<SelectListItem> list;

            if (user.HasAccessToAllBusinessUnits)
            {
                list = (from bu in _buService.GetAllNotDeleted() select bu).ToList()
                        .Select(bu => new SelectListItem() { Value = bu.BusinessUnitId.ToString(), Text = bu.Name }).ToList();
            }
            else if (user.BusinessUnits != null)
            {
                list = (from bu in user.BusinessUnits where bu.IsDeleted == false select bu).ToList()
                        .Select(bu => new SelectListItem() { Value = bu.BusinessUnitId.ToString(), Text = bu.Name }).ToList();
            }
            else
            {
                list = new List<SelectListItem>();
            }

            if (hasEmpty)
            {
                list.Insert(0, new SelectListItem() { Text = "", Value = "-1" });
            }
            //IList<ListItem> list = _services.SettingsService.GetResourceSupplyTypes();
            //

            return new SelectList(list, "Value", "Text");
        }

        /// <summary>
        /// Возвращает сотрудников доступных текущему пльзователю (по принадлежности к БЕ)
        /// </summary>
        protected virtual IEnumerable<Employee> GetEmployeesByUserAccess(User user, int pageSize, int pageNumber, string sortField, bool asc, out int totalRows)
        {
            IEnumerable<Employee> employees;
            totalRows = 0;

            if (user.HasAccessToAllBusinessUnits)
            {
                // Выбрать всех сотрудников
                employees = _employeeService.GetEmployees(pageSize, pageNumber, sortField, asc, out totalRows);
            }
            else
            {
                if (user.BusinessUnits == null || user.BusinessUnits.Count == 0)
                {
                    employees = new List<Employee>();
                }
                else
                {
                    IEnumerable<string> buNames = user.BusinessUnits.Select(bu => bu.Name);
                    employees = _employeeService.GetEmployees(buNames, pageSize, pageNumber, sortField, asc, out totalRows);
                }
            }

            return employees;
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            bool isControlMenuVisible = false;
            bool isViolationTypeSubmenuVisible = false;
            bool isBusinessUnitSubmenuVisible = false;
            bool isEmployeesSubmenuVisible = false;

            if (HttpContext.User.Identity.IsAuthenticated
                && _userService != null)
            {
                User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

                isControlMenuVisible = user.PermissionSet.UserManagementPermission >= Permission4.View;
                isViolationTypeSubmenuVisible = user.PermissionSet.ViolationTypeManagementPermission >= Permission3.View;
                isBusinessUnitSubmenuVisible = user.PermissionSet.BusinessUnitManagementPermission >= Permission3.View;
                isEmployeesSubmenuVisible = user.PermissionSet.EmployeeManagementPermission >= Permission4Import.View;
            }

            // Добавить во View данные по видимости элементов меню
            ViewData["IsControlMenuVisible"] = isControlMenuVisible;
            ViewData["IsDictionaryMenuVisible"] = isViolationTypeSubmenuVisible || isBusinessUnitSubmenuVisible || isEmployeesSubmenuVisible;
            ViewData["IsViolationTypeSubmenuVisible"] = isViolationTypeSubmenuVisible;
            ViewData["IsBusinessUnitSubmenuVisible"] = isBusinessUnitSubmenuVisible;
            ViewData["IsEmployeesSubmenuVisible"] = isEmployeesSubmenuVisible;
        }
    }
}