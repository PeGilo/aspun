﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aspun.Core.Contracts;
using Aspun.Core.Model;
using Aspun.Core.Services;
using Aspun.Web.ViewModels;
using Aspun.Web.AppCode.Mvc;

namespace Aspun.Web.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class UserController : ControllerBase
    {
        BusinessUnitService _buService;
        UserService _userService;

        public UserController(IAspunUow uow)
            : base(uow)
        {
            _buService = new BusinessUnitService(Uow);
            _userService = new UserService(Uow);
        }

        //
        // GET: /User/
        //[CustomAuthorizeAttribute(RequiredUserManagementPermission = Permission4.View)]
        //public ActionResult Index()
        //{
        //    IEnumerable<User> users = _userService.GetAll();
        //    List<UserItemModel> model = new List<UserItemModel>();

        //    foreach(User u in users)
        //    {
        //        model.Add(new UserItemModel(u));
        //    }

        //    return View(model);
        //}

        [CustomAuthorizeAttribute(RequiredUserManagementPermission = Permission4.View)]
        public ActionResult Index(int? page, string sort, string sortdir /* ASC/DESC */)
        {
            // Извлекаем параметры запроса
            string sortField = sort;
            bool asc = "ASC".Equals(sortdir, StringComparison.InvariantCultureIgnoreCase) ? true : false;

            User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

            // Заполняем модель
            PagedUsersModel model = new PagedUsersModel();
            model.IsCreateEnabled = user.PermissionSet.UserManagementPermission >= Permission4.InsertUpdate;
            model.IsDeleteEnabled = user.PermissionSet.UserManagementPermission >= Permission4.All;
            model.IsEditEnabled = user.PermissionSet.UserManagementPermission >= Permission4.InsertUpdate;

            model.PageSize = AppConfig.AppSettings.PageSizeUsers;
            model.PageNumber = page.HasValue ? page.Value : 1;

            int totalRows;
            IEnumerable<User> users = _userService.GetUsers(model.PageSize, model.PageNumber, sortField, asc, out totalRows);

            model.TotalRows = totalRows;
            foreach (User u in users)
            {
                model.Users.Add(new UserItemModel(u));
            }

            return View(model);
        }


        //
        // GET: /User/Create
        [CustomAuthorizeAttribute(RequiredUserManagementPermission = Permission4.InsertUpdate)]
        public ActionResult Create()
        {
            UserCreateModel model = new UserCreateModel();

            model.IsAllowed = true;

            return View(model);
        }

        //
        // POST: /User/Create

        [HttpPost]
        [CustomAuthorizeAttribute(RequiredUserManagementPermission = Permission4.InsertUpdate)]
        public ActionResult Create(UserCreateModel model, FormCollection collection)
        {
            ModelState.AddRuleViolations(model.GetRuleViolations(_userService));

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            User user = new User();
            //user.BusinessUnits = new List<BusinessUnit>();
            //user.BusinessUnits.Add(new BusinessUnit() { BusinessUnitId = 1, Name = "" });
            
            model.UpdateEntity(user);

            _userService.Create(user);

            //ModelState.AddModelError("HistoryPermissionRadio", "НЕправильное значени");

            return RedirectToAction("Index");
            //return View(model);
        }

        //
        // GET: /User/Edit/5
        [CustomAuthorizeAttribute(RequiredUserManagementPermission = Permission4.InsertUpdate)]
        public ActionResult Edit(int id)
        {
            User user = _userService.GetById(id);

            if (user == null)
            {
                return NotFound();
            }

            UserEditModel model = new UserEditModel(user);

            return View(model);
        }

        //
        // POST: /User/Edit/5
        [HttpPost]
        [CustomAuthorizeAttribute(RequiredUserManagementPermission = Permission4.InsertUpdate)]
        public ActionResult Edit(UserEditModel model, int id, FormCollection collection)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            User user = _userService.GetById(id);

            if (user == null)
            {
                return NotFound();
            }

            model.UpdateEntity(user);

            _userService.Update(user);

            return RedirectToAction("Index");
        }

        //
        // GET: /User/Delete/5
        [CustomAuthorizeAttribute(RequiredUserManagementPermission = Permission4.All)]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /User/Delete/5

        [HttpPost]
        [CustomAuthorizeAttribute(RequiredUserManagementPermission = Permission4.All)]
        public ActionResult Delete(int id, FormCollection collection)
        {
            _userService.DeleteUser(id);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Переопределенный метод для загрузки в модели общих данных
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            // Дополнить модель списком бизнес-единиц
            if (filterContext.Controller.ViewData.Model is ViewModels.UserBaseModel)
            {
                var model = (ViewModels.UserBaseModel)filterContext.Controller.ViewData.Model;
                model.BUList = GetBusinessUnitListNoCheck();
            }

            if (filterContext.Controller.ViewData.Model is ViewModels.UserCreateModel)
            {
                var model = (ViewModels.UserCreateModel)filterContext.Controller.ViewData.Model;
                model.BusinessUnitList = GetBusinessUnitList(true /* добавить пустой элемент */ );
            }
        }

        private IList<SelectListItem> GetBusinessUnitListNoCheck()
        {
            IList<SelectListItem> list = (from bu in _buService.GetAllNotDeleted() select bu).ToList()
                                        .Select(bu => new SelectListItem() { Value = bu.BusinessUnitId.ToString(), Text = bu.Name }).ToList();


            return list;
        }

    }
}
