﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aspun.Core.Model;

namespace Aspun.Web.ViewModels
{
    public class ViolationBriefModel
    {
        public int ViolationId { get; set; }

        public string Number { get; set; }

        public DateTime Date { get; set; }

        public string BusinessUnitName { get; set; }

        public string EmployeeFIO { get; set; }

        public string EmployeeNumber { get; set; }

        public string EmployeePosition { get; set; }

        public string EmployeeUnit { get; set; }

        public string ViolationTypeDescription { get; set; }

        public ViolationBriefModel()
        {
            
        }

        public ViolationBriefModel(Violation entity)
            : this()
        {
            ConvertEntity(entity);
        }

        private void ConvertEntity(Violation entity)
        {
            ViolationId = entity.ViolationId;
            Number = entity.Number;
            Date = entity.Date;
            BusinessUnitName = entity.BusinessUnitName;
            EmployeeFIO = entity.EmployeeFIO;
            EmployeeNumber = entity.EmployeeNumber;
            EmployeePosition = entity.EmployeePosition;
            EmployeeUnit = entity.EmployeeUnit;
            ViolationTypeDescription = entity.ViolationTypeDescription;
        }
    }
}