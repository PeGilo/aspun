﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aspun.Core.Model;
using Aspun.Core.Services;
using Aspun.Web.AppCode;
using Aspun.Web.AppCode.Mvc;


namespace Aspun.Web.ViewModels
{
    public class ViolationCreateModel : ViolationBaseModel
    {
        //[LocalizedDisplayNameAttribute("BusinessUnitName", NameResourceType = typeof(Resources.Names))]
        //[MaxLength(BusinessUnit.MAXL_NAME, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        //[Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "FieldIsRequired")]
        //public string BusinessUnitName { get; set; }

        [LocalizedDisplayNameAttribute("BusinessUnitName", NameResourceType = typeof(Resources.Names))]
        public int SelectedBusinessUnitId { get; set; }

        public virtual SelectList BusinessUnitList { get; set; }

        //[LocalizedDisplayNameAttribute("EmployeeFIO", NameResourceType = typeof(Resources.Names))]
        //[MaxLength(Employee.MAXL_FIO, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        //public string EmployeeFIO { get; set; }

        [LocalizedDisplayNameAttribute("EmployeeNumber", NameResourceType = typeof(Resources.Names))]
        [MaxLength(Employee.MAXL_Number, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        [Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "EmployeeIsNotSelected")]
        public string EmployeeNumber { get; set; }

        [LocalizedDisplayNameAttribute("EmployeePosition", NameResourceType = typeof(Resources.Names))]
        [MaxLength(Employee.MAXL_Position, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        [Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "EmployeeIsNotSelected")]
        public string EmployeePosition { get; set; }

        [LocalizedDisplayNameAttribute("EmployeeUnit", NameResourceType = typeof(Resources.Names))]
        [MaxLength(Employee.MAXL_Unit, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        //[Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "EmployeeIsNotSelected")]
        public string EmployeeUnit { get; set; }

        //
        //[MaxLength(ViolationType.MAXL_DESCRIPTION, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        //public string ViolationTypeDescription { get; set; }
        [LocalizedDisplayNameAttribute("Employee", NameResourceType = typeof(Resources.Names))]
        [MaxLength(Employee.MAXL_FIO, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        [Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "EmployeeIsNotSelected")]
        public string SelectedEmployee { get; set; }

        //[LocalizedDisplayNameAttribute("Employee", NameResourceType = typeof(Resources.Names))]
        //[Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "EmployeeIsNotSelected")]
        //public int? SelectedEmployeeId { get; set; }

        [LocalizedDisplayNameAttribute("SelectViolationType", NameResourceType = typeof(Resources.Names))]
        public int SelectedViolationId { get; set; }

        public virtual SelectList ViolationList { get; set; }


        public ViolationCreateModel()
        {

        }

        public override void UpdateEntity(Violation entity)
        {
            base.UpdateEntity(entity);

            //entity.BusinessUnitName = BusinessUnitName;
            entity.EmployeeFIO = SelectedEmployee;
            entity.EmployeeNumber = EmployeeNumber;
            entity.EmployeePosition = EmployeePosition;
            entity.EmployeeUnit = EmployeeUnit;
        }

        public override IEnumerable<RuleViolation> GetRuleViolations(ViolationService violationService)
        {
            IEnumerable<RuleViolation> baseRvs = base.GetRuleViolations(violationService);

            List<RuleViolation> rv = new List<RuleViolation>(baseRvs);

            return rv;
        }
    }
}