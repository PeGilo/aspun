﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspun.Web.ViewModels
{
    public class PagedViolationsModel
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int TotalRows { get; set; }
        public IList<ViolationBriefModel> Violations { get; set; }
        public bool UserCanEdit { get; set; }
        public bool IsCreateEnabled { get; set; }

        public PagedViolationsModel()
        {
            PageSize = 0;
            PageNumber = 0;
            TotalRows = 0;
            Violations = new List<ViolationBriefModel>();
            UserCanEdit = false;
        }
    }
}