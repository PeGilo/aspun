﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aspun.Core.Model;
using Aspun.Core.Services;
using Aspun.Web.AppCode;
using Aspun.Web.AppCode.Mvc;

namespace Aspun.Web.ViewModels
{
    public class ViolationDetailsModel
    {
        public int ViolationId { get; set; }

        [LocalizedDisplayNameAttribute("ViolationNumber", NameResourceType = typeof(Resources.Names))]
        public string Number { get; set; }

        [LocalizedDisplayNameAttribute("ViolationDate", NameResourceType = typeof(Resources.Names))]
        public String DateString { get; set; }

        [LocalizedDisplayNameAttribute("BusinessUnitName", NameResourceType = typeof(Resources.Names))]
        public string BusinessUnitName { get; set; }

        [LocalizedDisplayNameAttribute("EmployeeFIO", NameResourceType = typeof(Resources.Names))]
        public string EmployeeFIO { get; set; }

        [LocalizedDisplayNameAttribute("EmployeeNumber", NameResourceType = typeof(Resources.Names))]
        public string EmployeeNumber { get; set; }

        [LocalizedDisplayNameAttribute("EmployeePosition", NameResourceType = typeof(Resources.Names))]
        public string EmployeePosition { get; set; }

        [LocalizedDisplayNameAttribute("ViolationTypeCode", NameResourceType = typeof(Resources.Names))]
        public string ViolationTypeCode { get; set; }

        [LocalizedDisplayNameAttribute("ViolationTypeDescription", NameResourceType = typeof(Resources.Names))]
        public string ViolationTypeDescription { get; set; }

        [LocalizedDisplayNameAttribute("ViolationTypePar", NameResourceType = typeof(Resources.Names))]
        public string ViolationTypePar { get; set; }

        [LocalizedDisplayNameAttribute("ViolationDescription", NameResourceType = typeof(Resources.Names))]
        public string Description { get; set; }

        [LocalizedDisplayNameAttribute("ViolationCreatorUserName", NameResourceType = typeof(Resources.Names))]
        public string CreatorUserName { get; set; }

        public ViolationDetailsModel()
        {

        }

        public ViolationDetailsModel(Violation entity)
        {
            ConvertEntity(entity);
        }


        protected void ConvertEntity(Violation entity)
        {
            ViolationId = entity.ViolationId;
            Number = entity.Number;
            DateString = ConvertHelper.ToString(entity.Date);
            BusinessUnitName = entity.BusinessUnitName;
            EmployeeFIO = entity.EmployeeFIO;
            EmployeeNumber = entity.EmployeeNumber;
            EmployeePosition = entity.EmployeePosition;
            ViolationTypeDescription = entity.ViolationTypeDescription;
            ViolationTypePar = entity.ViolationTypePar;
            ViolationTypeCode = entity.ViolationTypeCode;
            Description = entity.Description;
            CreatorUserName = entity.CreatorUserName;
        }
    }
}