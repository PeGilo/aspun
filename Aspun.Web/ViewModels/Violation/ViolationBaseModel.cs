﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aspun.Core.Model;
using Aspun.Core.Services;
using Aspun.Web.AppCode.Mvc;
using Aspun.Web.AppCode;
using System.ComponentModel.DataAnnotations;

namespace Aspun.Web.ViewModels
{
    public class ViolationBaseModel
    {
        public virtual int ViolationId { get; set; }



        [UIHint("DateTimePicker")]
        //[DataType(DataType.Date, ErrorMessage = "Невозможно распознать указанную дату")]
        [DatePickerValidatorAttribute(ErrorMessageResourceName = "DateIsNotValid", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        [LocalizedDisplayNameAttribute("ViolationDate", NameResourceType = typeof(Resources.Names))]
        [Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "FieldIsRequired")]
        public virtual String DateString { get; set; }

        [LocalizedDisplayNameAttribute("ViolationDescription", NameResourceType = typeof(Resources.Names))]
        [MaxLength(Violation.MAXL_DESCRIPTION, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        public string Description { get; set; }

        public ViolationBaseModel ()
	    {
                
	    }

        public ViolationBaseModel(Violation entity)
            : this()
        {
            ConvertEntity(entity);
        }

        public virtual void UpdateEntity(Violation entity)
        {
            DateTime date;


            entity.Date = (ConvertHelper.TryParseDateTime(DateString, out date)) ? date : DateTime.Now;
            entity.Description = Description;
        }

        protected virtual void ConvertEntity(Violation entity)
        {
            ViolationId = entity.ViolationId;

            DateString = ConvertHelper.ToString(entity.Date);
            Description = entity.Description;
        }

        public virtual IEnumerable<RuleViolation> GetRuleViolations(ViolationService violationService)
        {
            List<RuleViolation> rv = new List<RuleViolation>();

            return rv;
        }
    }
}
