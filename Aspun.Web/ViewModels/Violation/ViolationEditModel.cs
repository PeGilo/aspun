﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aspun.Core.Model;
using Aspun.Core.Services;
using Aspun.Web.AppCode;
using Aspun.Web.AppCode.Mvc;

namespace Aspun.Web.ViewModels
{
    public class ViolationEditModel : ViolationBaseModel
    {
        [LocalizedDisplayNameAttribute("ViolationNumber", NameResourceType = typeof(Resources.Names))]
        //[MaxLength(Violation.MAXL_Number, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        //[Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "FieldIsRequired")]
        public virtual string Number { get; set; }

        [UIHint("ReadOnly")]
        [LocalizedDisplayNameAttribute("BusinessUnitName", NameResourceType = typeof(Resources.Names))]
        public string BusinessUnitName { get; set; }

        [UIHint("ReadOnly")]
        [LocalizedDisplayNameAttribute("EmployeeFIO", NameResourceType = typeof(Resources.Names))]
        public string EmployeeFIO { get; set; }

        [UIHint("ReadOnly")]
        [LocalizedDisplayNameAttribute("EmployeeNumber", NameResourceType = typeof(Resources.Names))]
        public string EmployeeNumber { get; set; }

        [UIHint("ReadOnly")]
        [LocalizedDisplayNameAttribute("EmployeePosition", NameResourceType = typeof(Resources.Names))]
        public string EmployeePosition { get; set; }

        [UIHint("ReadOnly")]
        [LocalizedDisplayNameAttribute("EmployeeUnit", NameResourceType = typeof(Resources.Names))]
        public string EmployeeUnit { get; set; }

        //[LocalizedDisplayNameAttribute("ViolationTypeDescription", NameResourceType = typeof(Resources.Names))]
        //public int SelectedViolationId { get; set; }

        //public virtual SelectList ViolationList { get; set; }

        [LocalizedDisplayNameAttribute("ViolationTypeCode", NameResourceType = typeof(Resources.Names))]
        [MaxLength(ViolationType.MAXL_CODE, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        [Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "FieldIsRequired")]
        public string ViolationTypeCode { get; set; }

        [LocalizedDisplayNameAttribute("ViolationTypeDescription", NameResourceType = typeof(Resources.Names))]
        [MaxLength(ViolationType.MAXL_DESCRIPTION, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        [Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "FieldIsRequired")]
        public string ViolationTypeDescription { get; set; }

        [LocalizedDisplayNameAttribute("ViolationTypePar", NameResourceType = typeof(Resources.Names))]
        [MaxLength(ViolationType.MAXL_REGULATIONPAR, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        //[Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "FieldIsRequired")]
        public string ViolationTypePar { get; set; }

        [UIHint("ReadOnly")]
        [LocalizedDisplayNameAttribute("ViolationCreatorUserName", NameResourceType = typeof(Resources.Names))]
        public string CreatorUserName { get; set; }

        public bool IsDeleteEnabled { get; set; }

        public ViolationEditModel()
        {

        }

        public ViolationEditModel(Violation entity)
            :base(entity)
        {
        }


        protected override void ConvertEntity(Violation entity)
        {
            base.ConvertEntity(entity);

            Number = entity.Number;
            BusinessUnitName = entity.BusinessUnitName;
            EmployeeFIO = entity.EmployeeFIO;
            EmployeeNumber = entity.EmployeeNumber;
            EmployeePosition = entity.EmployeePosition;
            EmployeeUnit = entity.EmployeeUnit;
            ViolationTypeDescription = entity.ViolationTypeDescription;
            ViolationTypePar = entity.ViolationTypePar;
            ViolationTypeCode = entity.ViolationTypeCode;
            CreatorUserName = entity.CreatorUserName;
        }

        public override void UpdateEntity(Violation entity)
        {
            base.UpdateEntity(entity);

            // Проапдейтить только ViolationType
            entity.ViolationTypeDescription = ViolationTypeDescription;
            entity.ViolationTypePar = ViolationTypePar;
            entity.ViolationTypeCode = ViolationTypeCode;
        }

        public override IEnumerable<RuleViolation> GetRuleViolations(ViolationService violationService)
        {
            IEnumerable<RuleViolation> baseRvs = base.GetRuleViolations(violationService);

            List<RuleViolation> rv = new List<RuleViolation>(baseRvs);

            return rv;
        }
    }
}