﻿using Aspun.Core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Aspun.Web.ViewModels
{
    public class BusinessUnitEditModel
    {
        public int BusinessUnitId { get; set; }

        [DisplayNameAttribute("Наименование")]
        [Required]
        [MaxLength(BusinessUnit.MAXL_NAME)]
        public string Name { get; set; }

        [DisplayNameAttribute("Префикс номера")]
        [Required]
        [MaxLength(Violation.MAXL_PREFIX)]
        public string Prefix { get; set; }

        public BusinessUnitEditModel()
        {
        }

        public BusinessUnitEditModel(BusinessUnit entity)
            : this()
        {
            ConvertEntity(entity);
        }

        public void UpdateEntity(BusinessUnit entity, bool updateId = false)
        {
            if (updateId)
            {
                entity.BusinessUnitId = BusinessUnitId;
            }
            entity.Name = Name;
            entity.Prefix = Prefix;
        }

        private void ConvertEntity(BusinessUnit entity)
        {
            BusinessUnitId = entity.BusinessUnitId;
            Name = entity.Name;
            Prefix = entity.Prefix;
        }
    }
}