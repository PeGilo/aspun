﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aspun.Common;
using Aspun.Core.Model;
using Aspun.Core.Services;
using Aspun.Web.AppCode.Mvc;

namespace Aspun.Web.ViewModels
{
    public class EmployeeCreateModel : EmployeeBaseModel
    {
        public EmployeeCreateModel()
        {

        }

        public EmployeeCreateModel(Employee entity)
            : base(entity)
        {

        }

        [LocalizedDisplayNameAttribute("EmployeeNumber", NameResourceType = typeof(Resources.Names))]
        [MaxLength(Employee.MAXL_Number, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        [Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "FieldIsRequired")]
        public virtual string Number { get; set; }

        [LocalizedDisplayNameAttribute("EmployeeBusinessUnit", NameResourceType = typeof(Resources.Names))]
        [MaxLength(Employee.MAXL_BusinessUnit, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        [Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "FieldIsRequired")]
        public virtual string BusinessUnit { get; set; }

        public virtual SelectList BusinessUnitList { get; set; }

        public override IEnumerable<RuleViolation> GetRuleViolations(EmployeeService employeeService)
        {
            IEnumerable<RuleViolation> baseRvs = base.GetRuleViolations(employeeService);

            List<RuleViolation> rv = new List<RuleViolation>(baseRvs);

            // Проверить уникальность логина пользователя
            if (!String.IsNullOrEmpty(Number) && !String.IsNullOrEmpty(BusinessUnit))
            {
                Employee emp = employeeService.GetEmployeeBy(Number, BusinessUnit);
                if (emp != null)
                {
                    // Сотрудник уже существует, значит нарушено правило уникальности
                    rv.Add(new RuleViolation("Сотрудник с указанными табельным номером и бизнес единицей уже существует", ReflectionHelper.GetPropertyName<string>(() => Number)));
                    rv.Add(new RuleViolation("Сотрудник с указанными табельным номером и бизнес единицей уже существует", ReflectionHelper.GetPropertyName<string>(() => BusinessUnit)));
                }
            }

            return rv;
        }

        protected override void ConvertEntity(Employee entity)
        {
            base.ConvertEntity(entity);

            Number = entity.Number;
            BusinessUnit = entity.BusinessUnit;
            //BusinessUnitList.SelectedValue = entity.BusinessUnit;
            //foreach (var item in BusinessUnitList.Items)
            //{
            //    if(item
            //}
        }

        public override void UpdateEntity(Employee entity)
        {
            base.UpdateEntity(entity);

            entity.Number = StringUtils.Trim(Number);
            entity.BusinessUnit = StringUtils.Trim(BusinessUnit);
        }
    }
}