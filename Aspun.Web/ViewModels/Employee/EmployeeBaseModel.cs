﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Aspun.Common;
using Aspun.Core.Model;
using Aspun.Core.Services;
using Aspun.Web.AppCode.Mvc;

namespace Aspun.Web.ViewModels
{
    public class EmployeeBaseModel
    {
        public virtual int EmployeeId { get; set; }

        [DisplayNameAttribute("ФИО")]
        [MaxLength(Employee.MAXL_FIO, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        [Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "FieldIsRequired")]
        public virtual string FIO { get; set; }

        [DisplayNameAttribute("Должность")]
        [MaxLength(Employee.MAXL_Position, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        public virtual string Position { get; set; }

        [DisplayNameAttribute("Подразделение")]
        [MaxLength(Employee.MAXL_Unit, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        public virtual string Unit { get; set; }

        public EmployeeBaseModel ()
	    {
                
	    }

        public EmployeeBaseModel(Employee entity)
            : this()
        {
            ConvertEntity(entity);
        }

        public virtual void UpdateEntity(Employee entity)
        {
            entity.FIO = FIO;
            entity.Position = Position;
            entity.Unit = Unit;

        }

        protected virtual void ConvertEntity(Employee entity)
        {
            EmployeeId = entity.EmployeeId;
            FIO = StringUtils.Trim(entity.FIO);
            Position = StringUtils.Trim(entity.Position);
            Unit = StringUtils.Trim(entity.Unit);
        }

        public virtual IEnumerable<RuleViolation> GetRuleViolations(EmployeeService userService)
        {
            List<RuleViolation> rv = new List<RuleViolation>();

            return rv;
        }
    }
}