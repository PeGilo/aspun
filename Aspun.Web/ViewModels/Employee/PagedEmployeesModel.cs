﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aspun.Core.Model;

namespace Aspun.Web.ViewModels
{
    public class PagedEmployeesModel
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int TotalRows { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
        public bool IsCreateEnabled { get; set; }
        public bool IsImportEnabled { get; set; }
        public bool IsDeleteEnabled { get; set; }
        public bool IsEditEnabled { get; set; }

        public PagedEmployeesModel()
        {
            PageSize = 0;
            PageNumber = 0;
            TotalRows = 0;
            Employees = new List<Employee>();
        }
    }
}