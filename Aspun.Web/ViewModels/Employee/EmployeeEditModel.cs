﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Aspun.Common;
using Aspun.Core.Model;
using Aspun.Core.Services;
using Aspun.Web.AppCode.Mvc;

namespace Aspun.Web.ViewModels
{
    public class EmployeeEditModel : EmployeeBaseModel
    {
        public EmployeeEditModel()
        {

        }

        public EmployeeEditModel(Employee entity)
            : base(entity)
        {

        }

        [LocalizedDisplayNameAttribute("EmployeeNumber", NameResourceType = typeof(Resources.Names))]
        [UIHint("ReadOnly")]
        public virtual string Number { get; set; }

        [LocalizedDisplayNameAttribute("EmployeeBusinessUnit", NameResourceType = typeof(Resources.Names))]
        [UIHint("ReadOnly")]
        public virtual string BusinessUnit { get; set; }

        public override IEnumerable<RuleViolation> GetRuleViolations(EmployeeService employeeService)
        {
            IEnumerable<RuleViolation> baseRvs = base.GetRuleViolations(employeeService);

            List<RuleViolation> rv = new List<RuleViolation>(baseRvs);

            return rv;
        }

        protected override void ConvertEntity(Employee entity)
        {
            base.ConvertEntity(entity);

            Number = entity.Number;
            BusinessUnit = entity.BusinessUnit;
        }

        public override void UpdateEntity(Employee entity)
        {
            base.UpdateEntity(entity);

            // Не изменять эти данные при редактировании
            //entity.Number = StringUtils.Trim(Number);
            //entity.BusinessUnit = StringUtils.Trim(BusinessUnit);
        }
    }
}