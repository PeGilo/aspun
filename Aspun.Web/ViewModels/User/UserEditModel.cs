﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aspun.Core.Model;
using Aspun.Web.AppCode.Mvc;

namespace Aspun.Web.ViewModels
{
    public class UserEditModel : UserBaseModel
    {
        public UserEditModel()
        {

        }

        public UserEditModel(User entity)
            : base(entity)
        {

        }

        [LocalizedDisplayNameAttribute("UserLogin", NameResourceType=typeof(Resources.Names))]
        [UIHint("ReadOnly")]
        public virtual string Login { get; set; }

        [LocalizedDisplayNameAttribute("BusinessUnitName", NameResourceType = typeof(Resources.Names))]
        public string BusinessUnitName { get; set; }

        protected override void ConvertEntity(User entity)
        {
            base.ConvertEntity(entity);
            
            Login = entity.Login;
            BusinessUnitName = entity.Unit;
        }

        public override void UpdateEntity(User entity)
        {
            base.UpdateEntity(entity);
            // не обновлять Логин при редактировании
            // entity.Login = Login;
            entity.Unit = BusinessUnitName;

            // Обновление связей с бизнес единицами в "Пользователе"
            // Существующие БЕ не перезаписывать
            if (entity.BusinessUnits == null)
            {
                entity.BusinessUnits = new List<BusinessUnit>();
            }
            
            int selectedBusinessUnitId;
            List<int> selected = new List<int>();
            List<BusinessUnit> buToRemove = new List<BusinessUnit>();

            // Перевести строковые ID из контрола в целочисленные, для удобства
            foreach (string selectedBUValue in SelectedBU)
            {
                if (Int32.TryParse(selectedBUValue, out selectedBusinessUnitId))
                {
                    selected.Add(selectedBusinessUnitId);
                }
            }

            // проверить все связи с бизнес-единицами и при необходимости изменений, изменить их
            foreach (BusinessUnit storedBU in entity.BusinessUnits)
            {
                // Если связь есть в выбранных пользователем, то оставить
                // Если связи нет, в выбранных пользователем, то удалить
                // Остальные добавить в список

                int index = selected.IndexOf(storedBU.BusinessUnitId);
                if (index >= 0)
                {
                    selected.RemoveAt(index);
                    continue;
                }
                else
                {
                    buToRemove.Add(storedBU);
                }
            }

            // Теперь, которые остались в stored - добавить, а которые в buToRemove - удалить.
            foreach (var bu in buToRemove)
            {
                entity.BusinessUnits.Remove(bu);
            }
            foreach (int buId in selected)
            {
                entity.BusinessUnits.Add(new BusinessUnit() { BusinessUnitId = buId });
            }
        }
    }

    //public class UserEditModel
    //{
    //    [DisplayNameAttribute("Логин")]
    //    public string Login { get; set; }

    //    [DisplayNameAttribute("Имя")]
    //    public string Name { get; set; }

    //    [DisplayNameAttribute("Пользователь разрешен")]
    //    public bool IsAllowed { get; set; }

    //    [DisplayNameAttribute("Разрешен доступ ко всем бизнес единицам")]
    //    public bool HasAccessToAllBusinessUnits { get; set; }

    //    //[DisplayNameAttribute("Права на управление пользователями")]
    //    //public Permission4 UserManagementPermission { get; set; }
    //    //[DisplayNameAttribute("Права на управление списком нарушений")]
    //    //public Permission4 ViolationManagementPermission { get; set; }
    //    //[DisplayNameAttribute("Права на управление справочником типов нарушений")]
    //    //public Permission3 ViolationTypeManagementPermission { get; set; }
    //    //[DisplayNameAttribute("Права на управление справочником бизнес единиц")]
    //    //public Permission4 BusinessUnitManagementPermission { get; set; }
    //    //[DisplayNameAttribute("Права на управление списком сотрудников")]
    //    //public Permission3 EmployeeManagementPermission { get; set; }
    //    //[DisplayNameAttribute("Права на доступ к истории изменений")]
    //    //public Permission2 HistoryPermission { get; set; }

    //    /// <summary>
    //    /// Выбранные бизнес-единицы в списке
    //    /// </summary>
    //    public string[] SelectedBU { get; set; }

    //    /// <summary>
    //    /// Список бизнес-единиц
    //    /// </summary>
    //    public IList<SelectListItem> BUList { get; set; }

    //    //public SelectList SL
    //    //{
    //    //    get
    //    //    {
    //    //        return new SelectList(
    //    //        new[] { 
    //    //            new SelectListItem() { Value="0", Text="" },
    //    //            new SelectListItem() { Value="1", Text="ва" },
    //    //            new SelectListItem() { Value="2", Text="ываы" },
    //    //            new SelectListItem() { Value="3", Text="ываыв" }
    //    //        }, "Value", "Text");
                
    //    //    }
    //    //    set
    //    //    {
    //    //    }
    //    //}


    //    [DisplayNameAttribute("Права на управление пользователями")]
    //    public RadioButtonListViewModel<Permission4> UserManagementPermissionRadio { get; set; }

    //    [DisplayNameAttribute("Права на управление списком нарушений")]
    //    public RadioButtonListViewModel<Permission4> ViolationManagementPermissionRadio { get; set; }

    //    [DisplayNameAttribute("Права на управление справочником типов нарушений")]
    //    public RadioButtonListViewModel<Permission3> ViolationTypeManagementPermissionRadio { get; set; }

    //    [DisplayNameAttribute("Права на управление справочником бизнес единиц")]
    //    public RadioButtonListViewModel<Permission4> BusinessUnitManagementPermissionRadio { get; set; }

    //    [DisplayNameAttribute("Права на управление списком сотрудников")]
    //    public RadioButtonListViewModel<Permission3> EmployeeManagementPermissionRadio { get; set; }

    //    [DisplayNameAttribute("Права на доступ к истории изменений")]
    //    public RadioButtonListViewModel<Permission2> HistoryPermissionRadio { get; set; }

    //    public List<RadioButtonListItem<Permission2>> ListItems2 = new List<RadioButtonListItem<Permission2>>
    //        {
    //            new RadioButtonListItem<Permission2>{ Text = Resources.Strings.PermissionDenied, Value = Permission2.Denied},
    //            new RadioButtonListItem<Permission2>{ Text = Resources.Strings.PermissionView, Value = Permission2.View},
    //        };

    //    public List<RadioButtonListItem<Permission3>> ListItems3 = new List<RadioButtonListItem<Permission3>>
    //        {
    //            new RadioButtonListItem<Permission3>{ Text = Resources.Strings.PermissionDenied, Value = Permission3.Denied},
    //            new RadioButtonListItem<Permission3>{ Text = Resources.Strings.PermissionOnlyView, Value = Permission3.View},
    //            new RadioButtonListItem<Permission3>{ Text = Resources.Strings.PermissionInsertUpdateDelete, Value = Permission3.All}
    //        };

    //    public List<RadioButtonListItem<Permission4>> ListItems4 = new List<RadioButtonListItem<Permission4>>
    //        {
    //            new RadioButtonListItem<Permission4>{ Text = Resources.Strings.PermissionDenied, Value = Permission4.Denied},
    //            new RadioButtonListItem<Permission4>{ Text = Resources.Strings.PermissionOnlyView, Value = Permission4.View},
    //            new RadioButtonListItem<Permission4>{ Text = Resources.Strings.PermissionInsertUpdate, Value = Permission4.InsertUpdate},
    //            new RadioButtonListItem<Permission4>{ Text = Resources.Strings.PermissionInsertUpdateDelete, Value = Permission4.All}
    //        };

    //    public UserEditModel()
    //    {
    //        SelectedBU = new string[0];
    //        BUList = new List<SelectListItem>();

    //        // Default values
    //        //UserManagementPermission = Permission4.Denied;
    //        //ViolationManagementPermission = Permission4.Denied;
    //        //ViolationTypeManagementPermission = Permission3.Denied;
    //        //BusinessUnitManagementPermission = Permission4.Denied;
    //        //EmployeeManagementPermission = Permission3.Denied;
    //        //HistoryPermission = Permission2.Denied;

    //        UserManagementPermissionRadio = new RadioButtonListViewModel<Permission4>
    //        {
    //            Id = "UserManagementPermission",
    //            SelectedValue = Permission4.Denied,
    //            ListItems = ListItems4
    //        };

    //        ViolationManagementPermissionRadio = new RadioButtonListViewModel<Permission4>
    //        {
    //            Id = "ViolationManagementPermission",
    //            SelectedValue = Permission4.Denied,
    //            ListItems = ListItems4
    //        };

    //        ViolationTypeManagementPermissionRadio = new RadioButtonListViewModel<Permission3>
    //        {
    //            Id = "ViolationTypeManagementPermission",
    //            SelectedValue = Permission3.Denied,
    //            ListItems = ListItems3
    //        };

    //        BusinessUnitManagementPermissionRadio = new RadioButtonListViewModel<Permission4>
    //        {
    //            Id = "BusinessUnitManagementPermission",
    //            SelectedValue = Permission4.Denied,
    //            ListItems = ListItems4
    //        };

    //        EmployeeManagementPermissionRadio = new RadioButtonListViewModel<Permission3> {
    //            Id = "EmployeeManagementPermission",
    //            SelectedValue = Permission3.Denied,
    //            ListItems = ListItems3
    //        };

    //        HistoryPermissionRadio = new RadioButtonListViewModel<Permission2>
    //        {
    //            Id = "HistoryPermission",
    //            SelectedValue = Permission2.Denied,
    //            ListItems = ListItems2
    //        };
    //    }

    //    public UserEditModel(User entity)
    //        : this()
    //    {
    //        ConvertEntity(entity);
    //    }

    //    public void UpdateEntity(User entity)
    //    {
    //        entity.Login = Login;
    //        entity.Name = Name;
    //        entity.IsAllowed = IsAllowed;
    //        entity.HasAccessToAllBusinessUnits = HasAccessToAllBusinessUnits;
    //        entity.PermissionSet = new PermissionSet()
    //        {
    //            UserManagementPermission = UserManagementPermissionRadio.SelectedValue,
    //            ViolationManagementPermission = ViolationManagementPermissionRadio.SelectedValue,
    //            ViolationTypeManagementPermission = ViolationTypeManagementPermissionRadio.SelectedValue,
    //            BusinessUnitManagementPermission = BusinessUnitManagementPermissionRadio.SelectedValue,
    //            EmployeeManagementPermission = EmployeeManagementPermissionRadio.SelectedValue,
    //            HistoryPermission = HistoryPermissionRadio.SelectedValue
    //        };

    //         //Существующие БЕ не перезаписывать
    //        if (entity.BusinessUnits == null)
    //        {
    //            entity.BusinessUnits = new List<BusinessUnit>();
    //        }
    //         //1. Те, которых нет в списке добавить

    //         //2. Те, которые есть в списке, но которых нет в итоговом удалить.

    //        List<int> selected = new List<int>();

    //        int selectedBusinessUnitId;

    //        foreach (string buValue in SelectedBU)
    //        {
    //            //BusinessUnit buEntity;
    //            if (Int32.TryParse(buValue, out selectedBusinessUnitId))
    //            {
    //                //buEntity = BUList.Where(item=>item.Value
    //                entity.BusinessUnits.Add(new BusinessUnit() { BusinessUnitId = selectedBusinessUnitId });
    //                //entity.BusinessUnits.Add(BULis
    //            }
    //        }
    //    }

    //    private void ConvertEntity(User entity)
    //    {
    //        Login = entity.Login;
    //        Name = entity.Name;
    //        IsAllowed = entity.IsAllowed;
    //        HasAccessToAllBusinessUnits = entity.HasAccessToAllBusinessUnits;

    //        UserManagementPermissionRadio.SelectedValue = entity.PermissionSet.UserManagementPermission;
    //        ViolationManagementPermissionRadio.SelectedValue = entity.PermissionSet.ViolationManagementPermission;
    //        ViolationTypeManagementPermissionRadio.SelectedValue = entity.PermissionSet.ViolationTypeManagementPermission;
    //        BusinessUnitManagementPermissionRadio.SelectedValue = entity.PermissionSet.BusinessUnitManagementPermission;
    //        EmployeeManagementPermissionRadio.SelectedValue = entity.PermissionSet.EmployeeManagementPermission;
    //        HistoryPermissionRadio.SelectedValue = entity.PermissionSet.HistoryPermission;

    //        if (entity.BusinessUnits != null)
    //        {
    //            List<string> selectedBU = new List<string>();
    //            foreach (BusinessUnit bu in entity.BusinessUnits)
    //            {
    //                selectedBU.Add(bu.BusinessUnitId.ToString());
    //            }
    //            SelectedBU = selectedBU.ToArray();
    //        }
    //    }
    //}
}