﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aspun.Core.Model;
using Aspun.Core.Services;
using Aspun.Web.AppCode.Mvc;

namespace Aspun.Web.ViewModels
{
    public class UserBaseModel
    {
        public virtual int UserId { get; set; }

        [DisplayNameAttribute("Имя")]
        [MaxLength(User.MAXL_NAME, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        public virtual string Name { get; set; }



        //[DisplayNameAttribute("Имя")]
        //[MaxLength(User.MAXL_NAME, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        //public virtual string Name { get; set; }

        [DisplayNameAttribute("Пользователь разрешен")]
        public virtual bool IsAllowed { get; set; }

        [DisplayNameAttribute("Разрешен доступ ко всем предприятиям")]
        public virtual bool HasAccessToAllBusinessUnits { get; set; }

        /// <summary>
        /// Выбранные бизнес-единицы в списке
        /// </summary>
        public virtual string[] SelectedBU { get; set; }

        /// <summary>
        /// Список бизнес-единиц
        /// </summary>
        public virtual IList<SelectListItem> BUList { get; set; }


        [DisplayNameAttribute("Права на управление пользователями")]
        public virtual RadioButtonListViewModel<Permission4> UserManagementPermissionRadio { get; set; }

        [DisplayNameAttribute("Права на управление списком нарушений")]
        public virtual RadioButtonListViewModel<Permission4> ViolationManagementPermissionRadio { get; set; }

        [DisplayNameAttribute("Права на управление справочником типов нарушений")]
        public virtual RadioButtonListViewModel<Permission3> ViolationTypeManagementPermissionRadio { get; set; }

        [DisplayNameAttribute("Права на управление справочником предприятий")]
        public virtual RadioButtonListViewModel<Permission3> BusinessUnitManagementPermissionRadio { get; set; }

        [DisplayNameAttribute("Права на управление списком сотрудников")]
        public virtual RadioButtonListViewModel<Permission4Import> EmployeeManagementPermissionRadio { get; set; }

        [DisplayNameAttribute("Права на доступ к истории изменений")]
        public virtual RadioButtonListViewModel<Permission2> HistoryPermissionRadio { get; set; }

        // ListItems должны быть для каждого контрола свои, так как они меняются вместе с данными
        //

        public List<RadioButtonListItem<Permission2>> ListItems2_HistoryPermission = new List<RadioButtonListItem<Permission2>>
            {
                new RadioButtonListItem<Permission2>{ Text = Resources.Strings.PermissionDenied, Value = Permission2.Denied},
                new RadioButtonListItem<Permission2>{ Text = Resources.Strings.PermissionView, Value = Permission2.View},
            };

        public List<RadioButtonListItem<Permission3>> ListItems3_ViolationTypeManagement = new List<RadioButtonListItem<Permission3>>
            {
                new RadioButtonListItem<Permission3>{ Text = Resources.Strings.PermissionDenied, Value = Permission3.Denied},
                new RadioButtonListItem<Permission3>{ Text = Resources.Strings.PermissionOnlyView, Value = Permission3.View},
                new RadioButtonListItem<Permission3>{ Text = Resources.Strings.PermissionInsertUpdateDelete, Value = Permission3.All}
            };

        public List<RadioButtonListItem<Permission4Import>> ListItems4_EmployeeManagement = new List<RadioButtonListItem<Permission4Import>>
            {
                new RadioButtonListItem<Permission4Import>{ Text = Resources.Strings.PermissionDenied, Value = Permission4Import.Denied},
                new RadioButtonListItem<Permission4Import>{ Text = Resources.Strings.PermissionOnlyView, Value = Permission4Import.View},
                new RadioButtonListItem<Permission4Import>{ Text = Resources.Strings.PermissionInsertUpdateDelete, Value = Permission4Import.InsertUpdateDelete},
                new RadioButtonListItem<Permission4Import>{ Text = Resources.Strings.PermissionImport, Value = Permission4Import.InsertUpdateDeleteImport}
            };

        public List<RadioButtonListItem<Permission4>> ListItems4_UserManagement = new List<RadioButtonListItem<Permission4>>
            {
                new RadioButtonListItem<Permission4>{ Text = Resources.Strings.PermissionDenied, Value = Permission4.Denied},
                new RadioButtonListItem<Permission4>{ Text = Resources.Strings.PermissionOnlyView, Value = Permission4.View},
                new RadioButtonListItem<Permission4>{ Text = Resources.Strings.PermissionInsertUpdate, Value = Permission4.InsertUpdate},
                new RadioButtonListItem<Permission4>{ Text = Resources.Strings.PermissionInsertUpdateDelete, Value = Permission4.All}
            };

        public List<RadioButtonListItem<Permission4>> ListItems4_ViolationManagement = new List<RadioButtonListItem<Permission4>>
            {
                new RadioButtonListItem<Permission4>{ Text = Resources.Strings.PermissionDenied, Value = Permission4.Denied},
                new RadioButtonListItem<Permission4>{ Text = Resources.Strings.PermissionOnlyView, Value = Permission4.View},
                new RadioButtonListItem<Permission4>{ Text = Resources.Strings.PermissionInsertUpdate, Value = Permission4.InsertUpdate},
                new RadioButtonListItem<Permission4>{ Text = Resources.Strings.PermissionInsertUpdateDelete, Value = Permission4.All}
            };

        public List<RadioButtonListItem<Permission3>> ListItems3_BusinessUnitManagement = new List<RadioButtonListItem<Permission3>>
            {
                new RadioButtonListItem<Permission3>{ Text = Resources.Strings.PermissionDenied, Value = Permission3.Denied},
                new RadioButtonListItem<Permission3>{ Text = Resources.Strings.PermissionOnlyView, Value = Permission3.View},
                new RadioButtonListItem<Permission3>{ Text = Resources.Strings.PermissionInsertUpdateDelete, Value = Permission3.All}
            };

        public UserBaseModel()
        {
            SelectedBU = new string[0];
            BUList = new List<SelectListItem>();
            
            // Default values
            //UserManagementPermission = Permission4.Denied;
            //ViolationManagementPermission = Permission4.Denied;
            //ViolationTypeManagementPermission = Permission3.Denied;
            //BusinessUnitManagementPermission = Permission4.Denied;
            //EmployeeManagementPermission = Permission3.Denied;
            //HistoryPermission = Permission2.Denied;

            UserManagementPermissionRadio = new RadioButtonListViewModel<Permission4>
            {
                Id = "UserManagementPermission",
                SelectedValue = Permission4.Denied,
                ListItems = ListItems4_UserManagement
            };

            ViolationManagementPermissionRadio = new RadioButtonListViewModel<Permission4>
            {
                Id = "ViolationManagementPermission",
                SelectedValue = Permission4.View,
                ListItems = ListItems4_ViolationManagement
            };

            ViolationTypeManagementPermissionRadio = new RadioButtonListViewModel<Permission3>
            {
                Id = "ViolationTypeManagementPermission",
                SelectedValue = Permission3.View,
                ListItems = ListItems3_ViolationTypeManagement
            };

            BusinessUnitManagementPermissionRadio = new RadioButtonListViewModel<Permission3>
            {
                Id = "BusinessUnitManagementPermission",
                SelectedValue = Permission3.View,
                ListItems = ListItems3_BusinessUnitManagement
            };

            EmployeeManagementPermissionRadio = new RadioButtonListViewModel<Permission4Import> {
                Id = "EmployeeManagementPermission",
                SelectedValue = Permission4Import.View,
                ListItems = ListItems4_EmployeeManagement
            };

            HistoryPermissionRadio = new RadioButtonListViewModel<Permission2>
            {
                Id = "HistoryPermission",
                SelectedValue = Permission2.Denied,
                ListItems = ListItems2_HistoryPermission
            };
        }

        public UserBaseModel(User entity)
            : this()
        {
            ConvertEntity(entity);
        }

        public virtual void UpdateEntity(User entity)
        {
            //entity.Login = Login;
            entity.Name = Name;
            entity.IsAllowed = IsAllowed;
            entity.HasAccessToAllBusinessUnits = HasAccessToAllBusinessUnits;
            entity.PermissionSet = new PermissionSet()
            {
                UserManagementPermission = UserManagementPermissionRadio.SelectedValue,
                ViolationManagementPermission = ViolationManagementPermissionRadio.SelectedValue,
                ViolationTypeManagementPermission = ViolationTypeManagementPermissionRadio.SelectedValue,
                BusinessUnitManagementPermission = BusinessUnitManagementPermissionRadio.SelectedValue,
                EmployeeManagementPermission = EmployeeManagementPermissionRadio.SelectedValue,
                HistoryPermission = HistoryPermissionRadio.SelectedValue
            };


        }

        protected virtual void ConvertEntity(User entity)
        {
            UserId = entity.UserId;
            //Login = entity.Login;
            Name = entity.Name;
            IsAllowed = entity.IsAllowed;
            HasAccessToAllBusinessUnits = entity.HasAccessToAllBusinessUnits;

            UserManagementPermissionRadio.SelectedValue = entity.PermissionSet.UserManagementPermission;
            ViolationManagementPermissionRadio.SelectedValue = entity.PermissionSet.ViolationManagementPermission;
            ViolationTypeManagementPermissionRadio.SelectedValue = entity.PermissionSet.ViolationTypeManagementPermission;
            BusinessUnitManagementPermissionRadio.SelectedValue = entity.PermissionSet.BusinessUnitManagementPermission;
            EmployeeManagementPermissionRadio.SelectedValue = entity.PermissionSet.EmployeeManagementPermission;
            HistoryPermissionRadio.SelectedValue = entity.PermissionSet.HistoryPermission;

            if (entity.BusinessUnits != null)
            {
                List<string> selectedBU = new List<string>();
                foreach (BusinessUnit bu in entity.BusinessUnits)
                {
                    selectedBU.Add(bu.BusinessUnitId.ToString());
                }
                SelectedBU = selectedBU.ToArray();
            }
        }

        public virtual IEnumerable<RuleViolation> GetRuleViolations(UserService userService)
        {
            List<RuleViolation> rv = new List<RuleViolation>();

            return rv;
        }
    }
}