﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aspun.Core.Model;
using Aspun.Web.AppCode.Mvc;

namespace Aspun.Web.ViewModels
{
    public class UserItemModel
    {
        public int UserId { get; set; }

        public string Login { get; set; }

        public string Name { get; set; }

        public string Unit { get; set; }

        public bool IsAllowed { get; set; }

        public UserItemModel()
        {
            
        }

        public UserItemModel(User entity)
            : this()
        {
            ConvertEntity(entity);
        }

        private void ConvertEntity(User entity)
        {
            UserId = entity.UserId;
            Login = entity.Login;
            Name = entity.Name;
            IsAllowed = entity.IsAllowed;
            Unit = entity.Unit;
        }
    }
}