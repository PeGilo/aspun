﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aspun.Common;
using Aspun.Core.Model;
using Aspun.Core.Services;
using Aspun.Web.AppCode.Mvc;

namespace Aspun.Web.ViewModels
{
    public class UserCreateModel : UserBaseModel
    {
        public UserCreateModel()
        {

        }

        public UserCreateModel(User entity)
            : base(entity)
        {

        }

        [LocalizedDisplayNameAttribute("UserLogin", NameResourceType = typeof(Resources.Names))]
        [MaxLength(User.MAXL_LOGIN, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        [Required(ErrorMessageResourceType=typeof(Resources.ValidationStrings), ErrorMessageResourceName="FieldIsRequired")]
        public virtual string Login { get; set; }

        /// <summary>
        /// Предпритятие в котором находится сам пользователь
        /// </summary>
        [LocalizedDisplayNameAttribute("BusinessUnitName", NameResourceType = typeof(Resources.Names))]
        [MaxLength(BusinessUnit.MAXL_NAME, ErrorMessageResourceName = "MaxLengthExceeded", ErrorMessageResourceType = typeof(Resources.ValidationStrings))]
        //[Required(ErrorMessageResourceType = typeof(Resources.ValidationStrings), ErrorMessageResourceName = "FieldIsRequired")]
        public string BusinessUnitName { get; set; }

        /// <summary>
        /// Список выбора предприятий для обозначения кому принадлежит сам пользователь
        /// </summary>
        public virtual SelectList BusinessUnitList { get; set; }

        public override IEnumerable<RuleViolation> GetRuleViolations(UserService userService)
        {
            IEnumerable<RuleViolation> baseRvs = base.GetRuleViolations(userService);

            List<RuleViolation> rv = new List<RuleViolation>();

            // Проверить уникальность логина пользователя
            if (!String.IsNullOrEmpty(Login))
            {
                User user = userService.GetUserInfoByLogin(Login);
                if (user != null)
                {
                    // Пользователь уже существует, значит нарушено правило уникальности
                    rv.Add(new RuleViolation("Пользователь с таким логином уже существует.", ReflectionHelper.GetPropertyName<string>(() => Login)));
                }
            }

            return rv;
        }

        protected override void ConvertEntity(User entity)
        {
            base.ConvertEntity(entity);
            Login = entity.Login;
            BusinessUnitName = entity.Unit;
        }

        public override void UpdateEntity(User entity)
        {
            base.UpdateEntity(entity);
            entity.Login = StringUtils.Trim(Login);
            entity.Unit = BusinessUnitName;

            ////Существующие БЕ не перезаписывать
            //if (entity.BusinessUnits == null)
            //{
                entity.BusinessUnits = new List<BusinessUnit>();
            //}
            //1. Те, которых нет в списке добавить

            //2. Те, которые есть в списке, но которых нет в итоговом удалить.

            //List<int> selected = new List<int>();

            int selectedBusinessUnitId;

            foreach (string buValue in SelectedBU)
            {
                //BusinessUnit buEntity;
                if (Int32.TryParse(buValue, out selectedBusinessUnitId))
                {
                    //buEntity = BUList.Where(item=>item.Value
                    entity.BusinessUnits.Add(new BusinessUnit() { BusinessUnitId = selectedBusinessUnitId });
                    //entity.BusinessUnits.Add(BULis
                }
            }
        }

    }
}