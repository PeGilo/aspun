﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspun.Web.ViewModels
{
    public class PagedUsersModel
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int TotalRows { get; set; }
        public IList<UserItemModel> Users { get; set; }

        public bool IsEditEnabled { get; set; }
        public bool IsCreateEnabled { get; set; }
        public bool IsDeleteEnabled { get; set; }

        public PagedUsersModel()
        {
            PageSize = 0;
            PageNumber = 0;
            TotalRows = 0;
            Users = new List<UserItemModel>();
        }
    }
}