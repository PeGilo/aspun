﻿using Aspun.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspun.Web.ViewModels
{
    public class ViolationTypeListEditModel
    {
        public IList<ViolationTypeEditModel> Items { get; private set; }

        public bool IsCreateEnabled { get; set; }
        public bool IsSaveEnabled { get; set; }
        public bool IsDeleteEnabled { get; set; }

        public ViolationTypeListEditModel()
        {
            Items = new List<ViolationTypeEditModel>();
        }

        public ViolationTypeListEditModel(IEnumerable<ViolationType> list)
        {
            ConvertEntity(list);
        }

        public void UpdateEntity(List<ViolationType> list)
        {
            //List<BL.Electroload> newList = new List<BL.Electroload>();
            //foreach (var item in Items)
            //{
            //    BL.Electroload electroload = new BL.Electroload();

            //    item.UpdateEntity(electroload);
            //    newList.Add(electroload);
            //}
            //entity.Electroloads = newList;

            list.Clear();
            foreach (var vtem in Items)
            {
                ViolationType vt = new ViolationType();
                vtem.UpdateEntity(vt, true /* updateId */);
                list.Add(vt);
            }

        }

        private void ConvertEntity(IEnumerable<ViolationType> list)
        {
            Items = new List<ViolationTypeEditModel>();

            foreach (var vt in list)
            {
                Items.Add(new ViolationTypeEditModel(vt));
            }
        }
    }
}