﻿using Aspun.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspun.Web.ViewModels
{
    public class BusinessUnitListEditModel
    {
        public IList<BusinessUnitEditModel> Items { get; private set; }

        public bool IsCreateEnabled { get; set; }
        public bool IsSaveEnabled { get; set; }
        public bool IsDeleteEnabled { get; set; }

        public BusinessUnitListEditModel()
        {
            Items = new List<BusinessUnitEditModel>();
        }

        public BusinessUnitListEditModel(IEnumerable<BusinessUnit> list)
        {
            ConvertEntity(list);
        }

        public void UpdateEntity(List<BusinessUnit> list)
        {
            list.Clear();
            foreach (var vtem in Items)
            {
                BusinessUnit vt = new BusinessUnit();
                vtem.UpdateEntity(vt, true /* updateId */);
                list.Add(vt);
            }

        }

        private void ConvertEntity(IEnumerable<BusinessUnit> list)
        {
            Items = new List<BusinessUnitEditModel>();

            foreach (var vt in list)
            {
                Items.Add(new BusinessUnitEditModel(vt));
            }
        }
    }
}