﻿using Aspun.Core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Aspun.Web.ViewModels
{
    public class ViolationTypeEditModel
    {
        public int ViolationTypeId { get; set; }

        [DisplayNameAttribute("Описание")]
        [Required]
        [MaxLength(ViolationType.MAXL_DESCRIPTION)]
        public string Description { get; set; }

        [DisplayNameAttribute("Пункт правил")]
        //[Required]
        [MaxLength(ViolationType.MAXL_REGULATIONPAR)]
        public string RegulationsPar { get; set; }

        [DisplayNameAttribute("Шифр нарушения")]
        [Required]
        [MaxLength(ViolationType.MAXL_CODE)]
        public string Code { get; set; }


        public ViolationTypeEditModel()
        {

        }

        public ViolationTypeEditModel(ViolationType entity)
            : this()
        {
            ConvertEntity(entity);
        }

        public void UpdateEntity(ViolationType entity, bool updateId = false)
        {
            if (updateId)
            {
                entity.ViolationTypeId = ViolationTypeId;
            }
            entity.Description = Description;
            entity.RegulationsPar = RegulationsPar;
            entity.Code = Code;
        }

        private void ConvertEntity(ViolationType entity)
        {
            ViolationTypeId = entity.ViolationTypeId;
            Description = entity.Description;
            RegulationsPar = entity.RegulationsPar;
            Code = entity.Code;
        }
    }
}