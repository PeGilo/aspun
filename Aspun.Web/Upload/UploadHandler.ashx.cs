﻿using Aspun.Web.AppCode;
using Aspun.Web.AppCode.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Aspun.Web.AppCode.Mvc;
using Aspun.Core.Model;

namespace Aspun.Web.Upload
{
    /// <summary>
    /// Summary description for UploadHandler
    /// </summary>
    public class UploadHandler : IHttpHandler
    {
        private readonly JavaScriptSerializer js;

        private AppConfiguration _appConfig = new AppConfiguration();

        public UploadHandler()
        {
            js = new JavaScriptSerializer();
            js.MaxJsonLength = 41943040;
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.AddHeader("Pragma", "no-cache");
            context.Response.AddHeader("Cache-Control", "private, no-cache");

            HandleMethod(context);

            
        }

        // Handle request based on method
        private void HandleMethod(HttpContext context)
        {
            switch (context.Request.HttpMethod)
            {
                //case "HEAD":
                //case "GET":
                //    if (GivenFilename(context)) DeliverFile(context);
                //    else ListCurrentFiles(context);
                //    break;

                case "POST":
                case "PUT":
                    UploadFile(context);
                    break;

                //case "DELETE":
                //    DeleteFile(context);
                //    break;

                case "OPTIONS":
                    context.Response.AddHeader("Allow", "POST,PUT,OPTIONS");// "DELETE,GET,HEAD,POST,PUT,OPTIONS");
                    context.Response.StatusCode = 200;
                    break;

                default:
                    context.Response.ClearHeaders();
                    context.Response.StatusCode = 405; // Method not allowed
                    break;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        // Upload file to the server
        private void UploadFile(HttpContext context)
        {
            var headers = context.Request.Headers;

            UploadFileActionStatus status = UploadWholeFile(context);

            //if (string.IsNullOrEmpty(headers["X-File-Name"]))
            //{
            //    UploadWholeFile(context, statuses);
            //}
            //else
            //{
            //    UploadPartialFile(headers["X-File-Name"], context, statuses);
            //}

            SendResponse(context, status);
        }

        #region "Не используем загрузку по частям"
        //// Upload partial file
        //private void UploadPartialFile(string fileName, HttpContext context, List<FilesStatus> statuses)
        //{
        //    if (context.Request.Files.Count != 1) throw new HttpRequestValidationException("Attempt to upload chunked file containing more than one fragment per request");
        //    var inputStream = context.Request.Files[0].InputStream;
        //    var fullName = StorageRoot + Path.GetFileName(fileName);

        //    using (var fs = new FileStream(fullName, FileMode.Append, FileAccess.Write))
        //    {
        //        var buffer = new byte[1024];

        //        var l = inputStream.Read(buffer, 0, 1024);
        //        while (l > 0)
        //        {
        //            fs.Write(buffer, 0, l);
        //            l = inputStream.Read(buffer, 0, 1024);
        //        }
        //        fs.Flush();
        //        fs.Close();
        //    }
        //    statuses.Add(new FilesStatus(new FileInfo(fullName)));
        //}
        #endregion

        // Upload entire file
        [CustomAuthorizeAttribute(RequiredUserManagementPermission = Permission4.All)]
        private UploadFileActionStatus UploadWholeFile(HttpContext context)
        {
            UploadFileActionStatus status = new UploadFileActionStatus();

            if (context.Request.Files.Count == 0)
            {
                status.ErrorStatus = 1;
                status.ErrorMessage = ActionStatus.InnerError;
                return status;
            }

            HttpPostedFile file = context.Request.Files[0];

            if (!FileManager.IsAcceptedExtension(file.FileName))
            {
                status.ErrorStatus = 1;
                status.ErrorMessage = ActionStatus.InvalidExtension;
                return status;
            }

            try
            {
                FileManager fm = new FileManager(_appConfig);

                string serverName = fm.StoreFile(file);

                return new UploadFileActionStatus()
                {
                    DisplayFileName = Path.GetFileName(file.FileName),
                    ServerFileName = serverName,
                    FileSize = file.ContentLength
                };
            }
            catch (Exception ex)
            {
                Logger.Error("Ошибка при загрузке файла на сервер", ex);

                status.ErrorStatus = 1;
                status.ErrorMessage = ActionStatus.InnerError;
                return status;
            }
        }

        private void SendResponse(HttpContext context, UploadFileActionStatus status)
        {
            context.Response.AddHeader("Vary", "Accept");
            try
            {
                if (context.Request["HTTP_ACCEPT"].Contains("application/json"))
                    context.Response.ContentType = "application/json";
                else
                    context.Response.ContentType = "text/plain";
            }
            catch
            {
                context.Response.ContentType = "text/plain";
            }

            //var jsonObj = js.Serialize(status);

            // чтобы в camelCase
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            var serializedObject = JsonConvert.SerializeObject(status, Formatting.Indented, settings);

            context.Response.Write(serializedObject);
        }
    }
}