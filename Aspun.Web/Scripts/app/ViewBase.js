﻿// Base type for client-side Views
app.ViewBase = function () {

};

// TODO: Объединить две ветки if
app.ViewBase.prototype.initControls = function (container) {

    $("input.datefield", container).datepicker({ dateFormat: "dd.mm.yy" });

    //$("input.timefield", container).timepicker({});


    //$('input[type="text"]', container).tooltip({
    //    delay: 1000,
    //    bodyHandler: this.tooltipBodyHandler
    //});

    //$("input.autocomplete", container).autocomplete({
    //    source: this.autocomplete_source
    //});

    //// В текстовых поля должны удалять пробелы и пр. в начале и конце строки при Ctrl+V
    //$("input[type='text']:not([data-val-number],[data-val-hyphennumber],[data-val-hypheninteger])", container).change(function () {
    //    $el = $(this);
    //    $el.val(app.Utils.trim($el.val()));
    //});

    //// В числовых полях должны удалять пробелы и пр. в начале и конце строки при Ctrl+V, а также внутренние пробелы
    //$("input[type='text'][data-val-number],input[type='text'][data-val-hyphennumber],input[type='text'][data-val-hypheninteger]", container).change(function () {
    //    $el = $(this);
    //    $el.val(app.Utils.removespaces($el.val()));
    //});
};


app.ViewBase.prototype.onLoad = function () {

    // Инициализировать культуру UI
    Globalize.culture('ru-RU');

    //// Локализуем валидацию на числа
    //$.validator.methods.number = function (value, element) {
    //    return this.optional(element) || !isNaN(Globalize.parseFloat(value));
    //}

    // Инициализировать плагин DatePicker
    $.datepicker.setDefaults($.datepicker.regional[app.currentLanguage]);

    //// Инициализировать плагин TimePicker
    //$.timepicker.setDefaults($.timepicker.regional[app.currentLanguage]);

    //// Инициализировать плагин autocalc
    //$(this).autocalc('init', {
    //    fractionDigits: 2,
    //    fractionSeparator: ',',
    //    emptyText: 'n/a'
    //});
};

app.ViewBase.prototype.onUnload = function () {
};

app.ViewBase.prototype.tooltipBodyHandler = function () {
    // Выбрать элемент валидации, содержащий текст ошибки
    var span = $(this).parent().next(".validationmessage").find("span:not(:empty):first");

    if (span.length > 0) {
        return span.text();
    }
    else {
        var title = $(this).attr('title');
        return title || "";
    }
};

app.ViewBase.prototype.getValidationMessage = function () {
    var message = '';
    $('.validation-summary-errors li').each(function () {
        message += $(this).text() + "<br/>";
    });
    return message;
};

app.ViewBase.prototype.hideValidationMessage = function () {
    $('.validation-summary-errors').addClass('invisible');
};

//app.ViewBase.prototype.showErrorMessage = function (message) {
//    alert(message);
//};

// Изменяет цифры в атрибутах (id, name, data-valmsg-for) элементов (input, span) 
// так, чтобы они шли по порядку.
app.ViewBase.prototype.reindexElementCollection = function (collection) {
    for (var i = 0; i < collection.size(); i++) {
        var row = collection[i];

        $(row).find("input, select").each(function (index, el) {

            $(el).attr('name', function (ind, old_name) {
                if (old_name) {
                    return old_name.replace(/\[\d+\]/, "[" + i + "]"); // change name
                }
                else
                    return '';
            })
            .attr('name', function (ind, old_name) {
                if (old_name) {
                    return old_name.replace(/\d+/, i.toString()); // in case the name has just a digit
                }
                else
                    return '';
            })
            .attr('id', function (ind, old_id) {
                if (old_id) {
                    return old_id.replace(/_\d+_/, "_" + i + "_");   // change id
                }
                return '';
            });
        });

        $(row).find("span[data-valmsg-for]").each(function (index, el) {
            $(el).attr('data-valmsg-for', function (ind, old_val) {
                if (old_val) {
                    return old_val.replace(/\[\d+\]/, "[" + i + "]"); // change name
                }
                else
                    return '';
            })
        });
    }
};

//app.ViewBase.prototype.autocomplete_source = function (request, response) {
//    $.ajax({
//        url: this.element.attr("data-source-url"),
//        dataType: "json",
//        data: {
//            term: request.term
//        },
//        success: function (data) {
//            response(data);
//        }
//    });
//};

// Валидация
//------------------------------------------------------//

//// Подсчитывает количество не валидных элементы с атрибутом WeakRequired.
//// @validator - jQuery объект validator формы
//// Возвращает количество
//app.ViewBase.prototype.invalidWeakConditions = function (validator) {

//    var countInvalidWeakConditions = 0;
//    // Найти все элементы с аттрибутами data-val-weak...
//    var elements = $('[data-val-weakrequired]');

//    elements.each(function (index, value) {
//        var validationStatuses = validator.ruleValidationStatus($(value));

//        if (validationStatuses.jqweakrequired === false) {
//            countInvalidWeakConditions++;
//        }
//    });
//    return countInvalidWeakConditions;
//};

// Обновляет валидацию формы в случае, если изменился DOM.
// @formSelector - селектор формы
app.ViewBase.prototype.updateFormValidation = function (formSelector) {
    $(formSelector).removeData("validator");
    $(formSelector).removeData("unobtrusiveValidation");
    $.validator.unobtrusive.parse(formSelector);
};

