﻿// общие методы для приложения
var app = (function () {

    var currentLanguage = "ru";

    var msgError = function (message) {
        $.jGrowl(message, { theme: 'error', sticky: true });
    };

    var resources = {
        ajaxErrorMessage: "Произошла ошибка во время обращения к серверу."
    };

    return {
        currentLanguage: currentLanguage,
        msgError: msgError,
        resources: resources
    };
})();