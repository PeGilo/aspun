﻿using System.Web;
using System.Web.Optimization;

namespace Aspun.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/plugins/globalize.js",
                        "~/Scripts/jquery.blockUI.js",
                        "~/Scripts/jquery.jgrowl.js",
                        "~/Scripts/plugins/jquery.ez-pinned-footer.js",
                        "~/Scripts/plugins/jquery.ui.datepicker-ru.js"
                        //"~/Scripts/plugins/jquery.tooltip.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/fileupload").Include(
                        "~/Scripts/plugins/jquery.iframe-transport.js",
                        "~/Scripts/plugins/jquery.fileupload.js",
                        "~/Scripts/plugins/jquery.fileupload-fp.js",
                        "~/Scripts/plugins/jquery.fileupload-ui.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                        "~/Scripts/app/inheritance.js",
                        "~/Scripts/app/app.js",
                        "~/Scripts/app/ViewBase.js",
                        "~/Scripts/app/DynamicListView.js"
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-responsive.css",
                "~/Content/jquery.jgrowl.css",
                "~/Content/site.css"
                ));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}