﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//using Microsoft.Office.Core;
//using Microsoft.Office.Interop;
using Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace Aspun.Web.AppCode.Excel
{
    public class ExcelReader
    {
        private string _fullFileName;
        private ExcelLoadParams _parameters;

        public ExcelReader(string fullFileName, ExcelLoadParams parameters)
        {
            _fullFileName = fullFileName;
            _parameters = parameters;
        }

        public IEnumerable<object[]> GetRecords()
        {
            Application xlapp = null;
            System.Globalization.CultureInfo oldCi = null;
            try
            {
                oldCi = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ru-RU");


                // TODO: Обработать исключения:
                xlapp = new Application();

                //// Получаем ссылку на интерфейс IDispatch
                //Type excelType = Type.GetTypeFromProgID("Excel.Application");
                //// Запускаем Excel
                //var excel = Activator.CreateInstance(excelType);

                //// Открыть файл: Workbooks = Excel.Workbooks;
                //var workbooks = excel.GetType().InvokeMember(
                //    "Workbooks", BindingFlags.GetProperty, null, excel, null);

                //var workbook = workbooks.GetType().InvokeMember(
                //    "Open", BindingFlags.InvokeMethod, null, workbooks, new object[] { _fullFileName });

                xlapp.Visible = false;
                
                // TODO: Обработать исключения:
                Workbook wkb = xlapp.Workbooks.Open(_fullFileName,
                        0, true, Missing.Value, Missing.Value, Missing.Value, true,
                        Missing.Value, Missing.Value, false, false, Missing.Value, false, false, XlCorruptLoad.xlNormalLoad);

                //Workbook wkb = xlapp.Workbooks.Open(_fullFileName);
                //    //0, true, Missing.Value, Missing.Value, Missing.Value, true,
                //    //Missing.Value, Missing.Value, false, false, Missing.Value, false, false, XlCorruptLoad.xlNormalLoad);

                Worksheet wks = wkb.Worksheets[1] as Worksheet;

                int rowIndex = _parameters.StartRowIndex;
                int colIndex = _parameters.StartColumnIndex;

                // .Text - This gets the formatted value of a cell. 
                // .Range - This mostly gets the underlying value from the cell.
                // .Range2 - This works the same way as Range.Value, 
                //         - except that it does not check the cell format and convert to Date or Currency.

                do
                {
                    // get row values
                    string[] values = new string[5];
                    values[0] = (string)wks.Cells[rowIndex, _parameters.NumberIndex].Text;
                    values[1] = (string)wks.Cells[rowIndex, _parameters.FIOIndex].Text;
                    values[2] = (string)wks.Cells[rowIndex, _parameters.PositionIndex].Text;
                    values[3] = (string)wks.Cells[rowIndex, _parameters.UnitIndex].Text;
                    values[4] = (string)wks.Cells[rowIndex, _parameters.BusinessUnitIndex].Text;
                                                              
                    if (String.IsNullOrEmpty(values[0])
                        && String.IsNullOrEmpty(values[1])
                        && String.IsNullOrEmpty(values[2])
                        && String.IsNullOrEmpty(values[3])
                        && String.IsNullOrEmpty(values[4]))
                    {
                        break;
                    }

                    rowIndex++;

                    yield return values;

                } while (true);
            }
            finally
            {
                if (xlapp != null)
                {
                    if (xlapp.ActiveWorkbook != null)
                    {
                        xlapp.ActiveWorkbook.Close(false);
                    }
                    xlapp.Quit();
                }
                if (oldCi != null)
                {
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldCi;
                }
            }
        }
    }
}