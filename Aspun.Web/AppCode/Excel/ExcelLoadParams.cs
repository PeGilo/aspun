﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspun.Web.AppCode.Excel
{
    /// <summary>
    /// Параметры для загрузки данных из файла Excel.
    /// </summary>
    public class ExcelLoadParams
    {
        public ExcelLoadParams()
        {

        }

        /// <summary>
        /// Номер первой строки с данными
        /// </summary>
        public int StartRowIndex { get; set; }

        /// <summary>
        /// Номер первого столбца с данными
        /// </summary>
        public int StartColumnIndex { get; set; }

        /// <summary>
        /// Номер поля "Табельный номер"
        /// </summary>
        public int NumberIndex { get; set; }

        /// <summary>
        /// Номер поля "ФИО"
        /// </summary>
        public int FIOIndex { get; set; }

        /// <summary>
        /// Номер поля "Должность"
        /// </summary>
        public int PositionIndex { get; set; }

        /// <summary>
        /// Номер поля "Подразделение"
        /// </summary>
        public int UnitIndex { get; set; }

        /// <summary>
        /// Номер поля "Бизнес единица (Организаци)"
        /// </summary>
        public int BusinessUnitIndex { get; set; }
    }
}