﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspun.Web.AppCode
{
    public class ImplortEmployeesActionStatus : ActionStatus
    {
        public ImplortEmployeesActionStatus()
        {
            CountEmployees = 0;
            BusinessUnitList = new List<Tuple<string, bool>>();
            //HtmlLayout = "";
        }

        public virtual int CountEmployees { get; set; }
        public virtual IEnumerable<Tuple<string, bool>> BusinessUnitList { get; set; }
        //public virtual string HtmlLayout { get; set; }
    }
}