﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspun.Web.AppCode
{
    public class UploadFileActionStatus : ActionStatus
    {
        public UploadFileActionStatus()
        {
            DisplayFileName = "";
            ServerFileName = "";
            FileSize = 0;

        }

        public virtual string DisplayFileName { get; set; }
        public virtual string ServerFileName { get; set; }
        public virtual int FileSize { get; set; }

        
    }
}