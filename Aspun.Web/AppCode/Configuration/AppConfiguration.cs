﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Aspun.Web.AppCode.Configuration
{
    public class AppConfiguration
    {
        //Controller _controller;

        private AppSettings _appSettings;

        public AppSettings AppSettings
        {
            get
            {
                if (_appSettings == null)
                {
                    _appSettings = new AppSettings();

                    string filesFolder = WebConfigurationManager.AppSettings["FilesFolder"];

                    if (!String.IsNullOrEmpty(filesFolder))
                    {
                        //_appSettings.FilesFolder = Path.Combine(_controller.Server.MapPath("~"), filesFolder);
                        _appSettings.FilesFolder = Path.Combine(HttpContext.Current.Server.MapPath("~"), filesFolder);
                    }
                    else
                    {
                        throw new ApplicationException("No file folder setting");
                    }

                    _appSettings.ExcelStartRow = Int32.Parse(WebConfigurationManager.AppSettings["ExcelStartRow"]);
                    _appSettings.ExcelStartColumn = Int32.Parse(WebConfigurationManager.AppSettings["ExcelStartColumn"]);
                    _appSettings.ExcelNumberIndex = Int32.Parse(WebConfigurationManager.AppSettings["ExcelNumberIndex"]);
                    _appSettings.ExcelFIOIndex = Int32.Parse(WebConfigurationManager.AppSettings["ExcelFIOIndex"]);
                    _appSettings.ExcelPositionIndex = Int32.Parse(WebConfigurationManager.AppSettings["ExcelPositionIndex"]);
                    _appSettings.ExcelUnitIndex = Int32.Parse(WebConfigurationManager.AppSettings["ExcelUnitIndex"]);
                    _appSettings.ExcelBusinessUnitIndex = Int32.Parse(WebConfigurationManager.AppSettings["ExcelBusinessUnitIndex"]);

                    _appSettings.PageSizeUsers = Int32.Parse(WebConfigurationManager.AppSettings["PageSizeUsers"]);

                    _appSettings.ConnectionString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                }
                return _appSettings;
            }
        }

        public AppConfiguration(/*Controller controller*/)
        {
            //_controller = controller;
        }
    }
}