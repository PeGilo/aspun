﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspun.Web.AppCode.Configuration
{
    public class AppSettings
    {
        public AppSettings()
        {
        }

        public string FilesFolder { get; set; }

        public int ExcelStartRow { get; set; }
        public int ExcelStartColumn { get; set; }
        public int ExcelNumberIndex { get; set; }
        public int ExcelFIOIndex { get; set; }
        public int ExcelPositionIndex { get; set; }
        public int ExcelUnitIndex { get; set; }
        public int ExcelBusinessUnitIndex { get; set; }

        public int PageSizeUsers { get; set; }

        public string ConnectionString { get; set; }
    }
}