﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Aspun.Web.AppCode.Mvc
{
    public class DatePickerValidatorAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            String s = value as String;
            if (!String.IsNullOrEmpty(s))
            {
                DateTime date;
                return ConvertHelper.TryParseDateTime(s, out date);
            }
            else
            {
                return false;
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return base.FormatErrorMessage(name);
        }
    }
}