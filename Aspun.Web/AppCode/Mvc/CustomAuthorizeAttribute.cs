﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aspun.Core.Model;
using Aspun.Core.Contracts;
using Ninject;

namespace Aspun.Web.AppCode.Mvc
{
    /// <summary>
    /// Аттрибут для проверки прав пользователя по свойствам пользователя из базы.
    /// Свойства аттрибута задают _наименьшие_ права, которыми должен обладать пользователь для доступа.
    /// </summary>
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Минимальный уровень доступа, который требует выполняемое действие
        /// </summary>
        public Permission4 RequiredUserManagementPermission { get; set; }

        /// <summary>
        /// Минимальный уровень доступа, который требует выполняемое действие
        /// </summary>
        public Permission4 RequiredViolationManagementPermission { get; set; }

        /// <summary>
        /// Минимальный уровень доступа, который требует выполняемое действие
        /// </summary>
        public Permission3 RequiredViolationTypeManagementPermission { get; set; }

        /// <summary>
        /// Минимальный уровень доступа, который требует выполняемое действие
        /// </summary>
        public Permission3 RequiredBusinessUnitManagementPermission { get; set; }

        /// <summary>
        /// Минимальный уровень доступа, который требует выполняемое действие
        /// </summary>
        public Permission4Import RequiredEmployeeManagementPermission { get; set; }

        /// <summary>
        /// Минимальный уровень доступа, который требует выполняемое действие
        /// </summary>
        public Permission2 RequiredHistoryPermission { get; set; }

        [Inject]
        public IAspunUow Uow { get; set; }

        public CustomAuthorizeAttribute()
        {
            // По-умолчанию задаем самые низкие права, чтобы при использовании аттрибута без параметров, доступ был ко всему.
            RequiredUserManagementPermission = Permission4.Denied;
            RequiredViolationManagementPermission = Permission4.Denied;
            RequiredViolationTypeManagementPermission = Permission3.Denied;
            RequiredBusinessUnitManagementPermission = Permission3.Denied;
            RequiredEmployeeManagementPermission = Permission4Import.Denied;
            RequiredHistoryPermission = Permission2.Denied;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //var isAuthorized = base.AuthorizeCore(httpContext);
            //if (!isAuthorized)
            //{
            //    return false;
            //}
            if (String.IsNullOrEmpty(httpContext.User.Identity.Name))
            {
                return false;
            }

            if (Uow == null)
            {
                throw new InvalidOperationException("Uow is null.");
            }
            
            // Проверить, если реальные права пользователя больше, либо равны указанным в аттрибуте, то разрешить действие.
            PermissionSet permissions;
            bool isUserAllowed = GetUserRights(httpContext.User.Identity.Name, out permissions);
            
            if (permissions != null
                && isUserAllowed
                && permissions.UserManagementPermission >= RequiredUserManagementPermission
                && permissions.ViolationManagementPermission >= RequiredViolationManagementPermission
                && permissions.ViolationTypeManagementPermission >= RequiredViolationTypeManagementPermission
                && permissions.BusinessUnitManagementPermission >= RequiredBusinessUnitManagementPermission
                && permissions.EmployeeManagementPermission >= RequiredEmployeeManagementPermission
                && permissions.HistoryPermission >= RequiredHistoryPermission)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            if (filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.Result = new RedirectResult("~/error/accessdenied");
            }
        }

        /// <summary>
        /// Читает права пользователя из базы
        /// </summary>
        /// <param name="identityName"></param>
        /// <param name="permissions">return Permissions for user or null</param>
        /// <returns>is user allowed</returns>
        protected virtual bool GetUserRights(string identityName, out PermissionSet permissions)
        {
            User entity = Uow.Users.GetByLogin(identityName);
            if (entity != null)
            {
                permissions = entity.PermissionSet;
                return entity.IsAllowed;
            }
            else
            {
                permissions = null;
                return false;
            }
        }
    }
}