﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspun.Web.AppCode
{
    public class ActionStatus
    {
        public ActionStatus()
        {
            ErrorStatus = 0;
            ErrorMessage = String.Empty;
        }

        public virtual int ErrorStatus { get; set; }
        public virtual string ErrorMessage { get; set; }

        public static readonly string InnerError = "Внутрення ошибка сервера.";
        public static readonly string InvalidExtension = "Выбран неподходящий тип файла.";
        //public static UploadFileActionStatus InnerError
        //{
        //    get { 
        //        return new ActionStatus
        //    }
        //}
    }
}