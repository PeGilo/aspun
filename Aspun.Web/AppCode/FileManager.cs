﻿using Aspun.Web.AppCode.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Aspun.Web.AppCode
{
    public class FileManager
    {
        private static System.Collections.Generic.SortedSet<string> _acceptedExt = new SortedSet<string>() {
            "xls", "xlsx"
        };

        private AppConfiguration _appConfig;

        public FileManager(AppConfiguration config)
        {
            _appConfig = config;
        }

        public static bool IsAcceptedExtension(string fileName)
        {
            if (!String.IsNullOrEmpty(fileName))
            {
                return _acceptedExt.Contains(Path.GetExtension(fileName).TrimStart(new char[] { '.' }).ToLower());
            }
            return false;
        }

        public string GetFileFullName(string shortFileName)
        {
            return Path.Combine(_appConfig.AppSettings.FilesFolder, shortFileName);
        }

        /// <summary>
        /// Stores file in upload folder.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="originalName"></param>
        /// <returns>Short file name of saved file.</returns>
        public virtual String StoreFile(HttpPostedFile file)
        {
            // 1. Make unique name
            string shortFileName = GenerateUniqueName() + Path.GetExtension(file.FileName);

            // Check if the folder exists
            if (!Directory.Exists(_appConfig.AppSettings.FilesFolder))
            {
                Directory.CreateDirectory(_appConfig.AppSettings.FilesFolder);
            }

            string fullFileName = Path.Combine(_appConfig.AppSettings.FilesFolder, shortFileName);

            file.SaveAs(fullFileName);

            return shortFileName;
        }

        protected virtual string GenerateUniqueName()
        {
            // Template for name is GUID.ext
            return DateTime.Now.ToString("yyyy-MM-dd_HH-mm") + "_" + Guid.NewGuid().ToString();
        }

        public void DeleteFile(string shortFileName)
        {
            string fullFileName = GetFileFullName(shortFileName);

            try
            {
                if (File.Exists(fullFileName))
                {
                    File.Delete(GetFileFullName(shortFileName));
                }
            }
            catch (Exception)
            {
                // Игнорировать ошибки при удалении файла.
            }
        }
    }
}