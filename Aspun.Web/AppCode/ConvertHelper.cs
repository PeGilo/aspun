﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aspun.Web.AppCode
{
    public static class ConvertHelper
    {
        public static string ToString(DateTime date)
        {
            return date.ToString("dd.MM.yyyy");
        }

        public static bool TryParseDateTime(string text, out DateTime result)
        {
            return DateTime.TryParse(text, out result);
        }
    }
}