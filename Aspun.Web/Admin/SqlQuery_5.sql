ALTER PROCEDURE [dbo].[sp_InsertViolation]
(
	@Number as nvarchar(20),
	@Date as datetime2(7),
	@BusinessUnitName as nvarchar(250),
	@EmployeeFIO as nvarchar(80),
	@EmployeeNumber as nvarchar(15),
	@EmployeePosition as nvarchar(250),
	@ViolationTypeDescription as nvarchar(250),
	@ViolationTypePar as nvarchar(80),
	@ViolationTypeCode as nvarchar(80),
	@EmployeeUnit as nvarchar(250),
	@Description as nvarchar(1024),
	@CreatorUserName as nvarchar(80),
	@Prefix as nvarchar(20),
	@Index as int
) AS

BEGIN
	INSERT INTO Violation (Number, [Date], BusinessUnitName, EmployeeFIO, EmployeeNumber, EmployeePosition,
							ViolationTypeDescription, ViolationTypePar, ViolationTypeCode,
							[Description], CreatorUserName, EmployeeUnit, Prefix, [Index]) 
	VALUES( @Number, @Date, @BusinessUnitName, @EmployeeFIO, @EmployeeNumber, @EmployeePosition,
							@ViolationTypeDescription, @ViolationTypePar, @ViolationTypeCode,
							@Description, @CreatorUserName, @EmployeeUnit, @Prefix, @Index)
END