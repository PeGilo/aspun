﻿INSERT INTO [dbo].[User]
([Login], [Name], [IsAllowed], [PermissionSetSerialized], [HasAccessToAllBusinessUnits])
VALUES
('sibgenco\GilayanPS', 'Петр', 
1,
'<?xml version="1.0" encoding="utf-16"?>  
<PermissionSet xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">    
   <UserManagementPermission>All</UserManagementPermission>    
   <ViolationManagementPermission>All</ViolationManagementPermission>    
   <ViolationTypeManagementPermission>All</ViolationTypeManagementPermission>    
   <BusinessUnitManagementPermission>All</BusinessUnitManagementPermission>    
   <EmployeeManagementPermission>All</EmployeeManagementPermission>    
   <HistoryPermission>View</HistoryPermission>  
</PermissionSet>',
1
)