﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Aspun.Core.Services;
using Ninject;
using Aspun.Core.Contracts;
using Aspun.Core.Model;

namespace Aspun.Web.Reports
{
    public partial class Report1 : Ninject.Web.PageBase
    {
        private IAspunUow _uow;

        [Inject]
        public IAspunUow Uow
        {
            get { return _uow; }
            set { _uow = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BusinessUnitService buService = new BusinessUnitService(Uow);
            UserService userService = new UserService(Uow);

            IEnumerable<BusinessUnit> bunits = buService.GetAllNotDeleted();

            User user = userService.GetByLogin(Context.User.Identity.Name);

            //ObjectDataSource1.Ty
        }
    }

    public class Commands
    {
        public IEnumerable<Violation> GetAll()
        {
            var violations = new List<Violation>()
            {
                new Violation() { 
                    //Number="00001", 
                    Date = new DateTime(2013, 01, 9), 
                    BusinessUnitName = "Теплосеть",
                    EmployeeFIO = "Иванов Иван Иванович",
                    EmployeeNumber = "0923",
                    EmployeePosition = "Слесарь",
                    ViolationTypeDescription = "Алкогольное опьянение на рабочем месте",
                    ViolationTypePar = "пар. 5.1",
                    Description = "Пьянствовал на работе.",
                    CreatorUserName = "Петров Петр Петрович" },

                new Violation() { 
                    //Number="00002", 
                    Date = new DateTime(2013, 01, 10), 
                    BusinessUnitName = "Теплосеть",
                    EmployeeFIO = "Иванов Иван Иванович",
                    EmployeeNumber = "0923",
                    EmployeePosition = "Слесарь",
                    ViolationTypeDescription = "Курение в неположеном месте",
                    ViolationTypePar = "пар. 3.2",
                    Description = "Курил на рабочем месте.",
                    CreatorUserName = "Петров Петр Петрович" },

                new Violation() { 
                    //Number="00003", 
                    Date = new DateTime(2013, 02, 9), 
                    BusinessUnitName = "Абаканская ТЭЦ",
                    EmployeeFIO = "Сидоров Сидор",
                    EmployeeNumber = "0001",
                    EmployeePosition = "Слесарь",
                    ViolationTypeDescription = "Алкогольное опьянение на рабочем месте",
                    ViolationTypePar = "пар. 5.1",
                    Description = "Пьянствовал на работе.",
                    CreatorUserName = "Петров Петр Петрович" },

                new Violation() { 
                    //Number="00004", 
                    Date = new DateTime(2013, 02, 9), 
                    BusinessUnitName = "Абаканская ТЭЦ",
                    EmployeeFIO = "Сидоров Сидор",
                    EmployeeNumber = "0001",
                    EmployeePosition = "Слесарь",
                    ViolationTypeDescription = "Курение в неположеном месте",
                    ViolationTypePar = "пар. 3.2",
                    Description = "Курил на рабочем месте.",
                    CreatorUserName = "Романов Роман" },

                new Violation() { 
                    //Number="00005", 
                    Date = new DateTime(2013, 03, 10), 
                    BusinessUnitName = "Теплосеть",
                    EmployeeFIO = "Забегайло Зохан",
                    EmployeeNumber = "0005",
                    EmployeePosition = "Слесарь",
                    ViolationTypeDescription = "Работа без каски",
                    ViolationTypePar = "пар. 10.1",
                    Description = "Работал без каски.",
                    CreatorUserName = "Смирнов А.П." },

                new Violation() { 
                    //Number="00006", 
                    Date = new DateTime(2013, 03, 10), 
                    BusinessUnitName = "Теплосеть",
                    EmployeeFIO = "Жирнов Житомир",
                    EmployeeNumber = "0006",
                    EmployeePosition = "Монтажник",
                    ViolationTypeDescription = "Работа без каски",
                    ViolationTypePar = "пар. 10.1",
                    Description = "Работал без каски.",
                    CreatorUserName = "Смирнов А.П." },

                new Violation() { 
                    //Number="00007", 
                    Date = new DateTime(2013, 03, 11), 
                    BusinessUnitName = "Абаканская ТЭЦ",
                    EmployeeFIO = "Кузнецов Николай",
                    EmployeeNumber = "0008",
                    EmployeePosition = "Бригадир",
                    ViolationTypeDescription = "Работа без каски",
                    ViolationTypePar = "пар. 10.1",
                    Description = "Работал без каски.",
                    CreatorUserName = "Глазунов Георгий" }
            };

            return violations;
        }
    }
}