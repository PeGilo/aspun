﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Report1.aspx.cs" Inherits="Aspun.Web.Reports.Report1" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scm" runat="server">
        </asp:ScriptManager>

        <div>
            <%--        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="100%" ProcessingMode="Remote">
            <ServerReport ReportPath="/Aspun.Reports/ViolationList" ReportServerUrl="http://localhost/ReportServer_SQL2008R2" />
        </rsweb:ReportViewer>   --%>
        </div>

        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="400px" Width="400px">
            <LocalReport ReportPath="Reports\Report2.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="ViolationRow" />
                </DataSources>
                
            </LocalReport>
        </rsweb:ReportViewer>

<%--        <asp:GridView
            ID="GridView1"
            runat="server"
            DataSourceID="ObjectDataSource1" />--%>

        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAll"
            TypeName="Aspun.Web.Reports.Commands">

            <%--            <SelectParameters>
                <asp:Parameter DefaultValue="8" Name="StartDate" Type="DateTime" />
            </SelectParameters>--%>
        </asp:ObjectDataSource>

    </form>
</body>
</html>
