﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Common
{
    /// <summary>
    /// Содержит вспомогательные методы для работы с перечислениями.
    /// </summary>
    public static class EnumHelper
    {
        /// <summary>
        /// Ищет в ресурсах строку, соответствующую указанному значению перечисления.
        /// </summary>
        /// <param name="resourceManagerProvider"></param>
        /// <param name="enumerator"></param>
        /// <returns></returns>
        public static string GetDisplayName(Type resourceManagerProvider, Enum enumerator)
        {
            return GetDisplayName(resourceManagerProvider, enumerator.GetType(), enumerator.ToString());
        }

        /// <summary>
        /// Ищет в ресурсах строку, соответствующую указанному значению перечисления и указанному элементу перечисления.
        /// </summary>
        /// <param name="resourceManagerProvider"></param>
        /// <param name="enumType"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private static string GetDisplayName(Type resourceManagerProvider, Type enumType, string name)
        {
            if (enumType == null || !enumType.IsEnum)
            {
                throw new ArgumentException("enumType");
            }

            string resourceKey = String.Format("{0}_{1}", enumType.Name, name);
            string text = ResourceHelper.LookupResource(resourceManagerProvider/*typeof(Resources.Names)*/, resourceKey);

            if (text != null)
            {
                return text;
            }
            else
            {
                return name;
            }
        }

        /// <summary>
        /// Для указанного типа перечисления возвращает список, состоящий из его элементов,
        /// с соответствующими значениями Id.
        /// </summary>
        /// <param name="resourceManagerProvider"></param>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public static IList<ListItem> ToList(Type resourceManagerProvider, Type enumType)
        {
            List<ListItem> list = new List<ListItem>();

            foreach (var value in Enum.GetValues(enumType))
            {
                string displayName = GetDisplayName(resourceManagerProvider, enumType, Enum.GetName(enumType, value));

                list.Add(new ListItem((int)value, displayName));
            }

            return list;
        }

    }
}
