﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Common
{
    public static class StringUtils
    {
        public static string Substring(string s, int length)
        {
            if (s == null)
            {
                return String.Empty;
            }
            else if (s.Length <= length)
            {
                return s;
            }
            else
            {
                return s.Substring(0, length);
            }
        }

        public static string Substring(string s, int startIndex, int length)
        {
            if (s == null || s.Length <= startIndex)
            {
                return string.Empty;
            }
            else
            {
                length = Math.Min(length, s.Length - startIndex);
                return s.Substring(startIndex, length);
            }
        }

        public static string Trim(string s)
        {
            return String.IsNullOrEmpty(s) ? String.Empty : s.Trim();
        }
    }

}
