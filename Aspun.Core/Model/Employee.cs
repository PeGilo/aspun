﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Core.Model
{
    public class Employee
    {
        public const int MAXL_FIO = 80;
        public const int MAXL_Number = 15;
        public const int MAXL_Position = 250;
        public const int MAXL_Unit = 250;
        public const int MAXL_BusinessUnit = 80;

        public int EmployeeId { get; set; }

        [MaxLength(MAXL_FIO)]
        public string FIO { get; set; }

        [MaxLength(MAXL_Number)]
        public string Number { get; set; }

        [MaxLength(MAXL_Position)]
        public string Position { get; set; }

        [MaxLength(MAXL_Unit)]
        public string Unit { get; set; }

        [MaxLength(MAXL_BusinessUnit)]
        public string BusinessUnit { get; set; }

        public bool IsEmpty()
        {
            return
                (String.IsNullOrEmpty(FIO)
                && String.IsNullOrEmpty(Number)
                && String.IsNullOrEmpty(Position)
                && String.IsNullOrEmpty(Unit)
                && String.IsNullOrEmpty(BusinessUnit)
                );
        }
    }
}
