﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Core.Model
{
    public class BusinessUnit
    {
        public const int MAXL_NAME = 250;

        public int BusinessUnitId { get; set; }

        [StringLength(MAXL_NAME)]
        public string Name { get; set; }

        [MaxLength(Violation.MAXL_PREFIX)]
        public string Prefix { get; set; }

        public bool IsDeleted { get; set; }

        public BusinessUnit()
        {
            // присвоение значений по-умолчанию.
            IsDeleted = false;
        }
    }
}
