﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Core.Model
{
    public enum Permission2
    {
        Denied = 0,
        View = 1
    }

    public enum Permission3
    {
        Denied = 0,
        View = 1,
        All = 2
    }

    public enum Permission4
    {
        Denied = 0,
        View = 1,
        InsertUpdate = 2,
        All = 3
    }

    public enum Permission4Import
    {
        Denied = 0,
        View = 1,
        InsertUpdateDelete = 2,
        InsertUpdateDeleteImport = 3
    }


    public class PermissionSet
    {
        public Permission4 UserManagementPermission { get; set; }
        public Permission4 ViolationManagementPermission { get; set; }
        public Permission3 ViolationTypeManagementPermission { get; set; }
        public Permission3 BusinessUnitManagementPermission { get; set; }
        public Permission4Import EmployeeManagementPermission { get; set; }
        public Permission2 HistoryPermission { get; set; }
    }
}
