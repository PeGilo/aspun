﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Core.Model
{
    public class Violation
    {
        public const int MAXL_DESCRIPTION = 1024;
        public const int MAXL_Number = 20;
        public const int MAXL_PREFIX = 20;

        public int ViolationId { get; set; }

        [MaxLength(MAXL_PREFIX)]
        public string Prefix { get; set; }

        public int Index { get; set; }

        //[MaxLength(MAXL_Number)]
        public string Number
        {
            get
            {
                return Prefix + "-" + Index.ToString();
            }
            protected set { }
        }

        public DateTime Date { get; set; }

        [MaxLength(BusinessUnit.MAXL_NAME)]
        public string BusinessUnitName { get; set; }

        [MaxLength(Employee.MAXL_FIO)]
        public string EmployeeFIO { get; set; }

        [MaxLength(Employee.MAXL_Number)]
        public string EmployeeNumber { get; set; }

        [MaxLength(Employee.MAXL_Position)]
        public string EmployeePosition { get; set; }

        [MaxLength(Employee.MAXL_Unit)]
        public string EmployeeUnit { get; set; }

        [MaxLength(ViolationType.MAXL_DESCRIPTION)]
        public string ViolationTypeDescription { get; set; }

        [MaxLength(ViolationType.MAXL_REGULATIONPAR)]
        public string ViolationTypePar { get; set; }

        [MaxLength(ViolationType.MAXL_CODE)]
        public string ViolationTypeCode { get; set; }

        [MaxLength(MAXL_DESCRIPTION)]
        public string Description { get; set; }

        [MaxLength(User.MAXL_NAME)]
        public string CreatorUserName { get; set; }
    }
}
