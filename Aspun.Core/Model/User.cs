﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aspun.Common;

namespace Aspun.Core.Model
{
    public class User
    {
        public const int MAXL_LOGIN = 80;
        public const int MAXL_NAME = 80;

        public int UserId { get; set; }

        [StringLength(MAXL_LOGIN)]
        public string Login { get; set; }

        [StringLength(MAXL_NAME)]
        public string Name { get; set; }

        [MaxLength(BusinessUnit.MAXL_NAME)]
        public string Unit { get; set; }

        public bool IsAllowed { get; set; }

        public PermissionSet PermissionSet { get; set; }

        /// <summary>
        /// Поле создано для упрощения сохранения объекта в базу (чтобы сохранять в Xml)
        /// </summary>
        public string PermissionSetSerialized { get; set; }

        public bool HasAccessToAllBusinessUnits { get; set; }

        public ICollection<BusinessUnit> BusinessUnits { get; set; }
    }
}
