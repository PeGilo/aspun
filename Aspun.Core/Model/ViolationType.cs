﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Core.Model
{
    public class ViolationType
    {
        public const int MAXL_DESCRIPTION = 250;
        public const int MAXL_REGULATIONPAR = 80;
        public const int MAXL_CODE = 80;

        public int ViolationTypeId { get; set; }

        [StringLength(MAXL_DESCRIPTION)]
        public string Description { get; set; }

        [StringLength(MAXL_REGULATIONPAR)]
        public string RegulationsPar { get; set; }

        [StringLength(MAXL_CODE)]
        public string Code { get; set; }
    }
}
