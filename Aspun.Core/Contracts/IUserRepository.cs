﻿using Aspun.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Core.Contracts
{
    public interface IUserRepository : IRepository<User>
    {
        User GetByLogin(string login);
        User GetUserInfoByLogin(string login);
        IQueryable<User> GetUsers(int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords);
    }
}
