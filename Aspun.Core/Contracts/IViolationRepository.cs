﻿using Aspun.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Core.Contracts
{
    public interface IViolationRepository : IRepository<Violation>
    {
        IQueryable<Violation> GetAllViolations(int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords);
        IQueryable<Violation> GetViolationsByBU(IEnumerable<string> buNames, int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords);
    }
}
