﻿using Aspun.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Core.Contracts
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        void RemoveAll();

        IQueryable<Employee> GetAllEmployees(int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords);
        IQueryable<Employee> GetEmployeesByBU(IEnumerable<string> buNames, int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords);
        Employee GetEmployeeBy(string number, string businessUnit);

        IQueryable<Employee> SearchEmployees(int maxCount, IList<string> fieldsToSearch, string searchTerm);
        IQueryable<Employee> SearchEmployees(IEnumerable<string> buNames, int maxCount, IList<string> fieldsToSearch, string searchTerm);

    }
}
