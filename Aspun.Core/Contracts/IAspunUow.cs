﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Core.Contracts
{
    /// <summary>
    /// Interface for the "Unit of Work"
    /// </summary>
    public interface IAspunUow
    {
        // Save pending changes to the data store.
        void Commit();

        // Repositories
        IUserRepository Users { get; }
        IViolationRepository Violations { get; }
        IViolationTypeRepository ViolationTypes { get; }
        IEmployeeRepository Employees { get; }
        IBusinessUnitRepository BusinessUnits { get; }
    }
}
