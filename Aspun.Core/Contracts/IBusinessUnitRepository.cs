﻿using Aspun.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Core.Contracts
{
    public interface IBusinessUnitRepository : IRepository<BusinessUnit>
    {
        void UpdateAll(IList<BusinessUnit> newList);
        IQueryable<BusinessUnit> GetAllNotDeleted();
    }
}
