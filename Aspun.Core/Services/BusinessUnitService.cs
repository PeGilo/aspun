﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aspun.Core.Contracts;
using Aspun.Core.Model;

namespace Aspun.Core.Services
{
    public class BusinessUnitService
    {
        private IAspunUow _uow;

        public BusinessUnitService(IAspunUow uow)
        {
            _uow = uow;
        }

        public IQueryable<BusinessUnit> GetAllNotDeleted()
        {
            return _uow.BusinessUnits.GetAllNotDeleted();
        }

        public void UpdateList(List<BusinessUnit> newList)
        {
            _uow.BusinessUnits.UpdateAll(newList);
        }

        public BusinessUnit GetById(int id)
        {
            return _uow.BusinessUnits.GetById(id);
        }
    }
}
