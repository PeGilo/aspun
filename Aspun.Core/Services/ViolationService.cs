﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aspun.Common;
using Aspun.Core.Contracts;
using Aspun.Core.Model;

namespace Aspun.Core.Services
{
    public class ViolationService
    {
        private IAspunUow _uow;

        public ViolationService(IAspunUow uow)
        {
            _uow = uow;
        }

        public Violation GetById(int id)
        {
            return _uow.Violations.GetById(id);
        }

        public IQueryable<Violation> GetViolations(int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords)
        {
            return _uow.Violations.GetAllViolations(pageSize, pageNumber, sortField, asc, out totalRecords);
        }

        public IQueryable<Violation> GetViolations(IEnumerable<string> businessUnitName, int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords)
        {
            return _uow.Violations.GetViolationsByBU(businessUnitName, pageSize, pageNumber, sortField, asc, out totalRecords);
        }

        //public Violation GetViolationBy(string number, string businessUnit)
        //{
        //    return _uow.Violations.GetViolationBy(number, businessUnit);
        //}

        public void Create(Violation violation)
        {
            _uow.Violations.Add(violation);
            _uow.Commit();

            // Метод Add обновил индекс так что его можно использовать
        }

        public void Update(Violation violation)
        {
            _uow.Violations.Update(violation);
            _uow.Commit();
        }

        public void DeleteViolation(int id)
        {
            _uow.Violations.Delete(id);
            _uow.Commit();
        }
    }
}
