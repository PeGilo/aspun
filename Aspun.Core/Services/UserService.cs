﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aspun.Core.Contracts;
using Aspun.Core.Model;

namespace Aspun.Core.Services
{
    public class UserService
    {
        private IAspunUow _uow;

        public UserService(IAspunUow uow)
        {
            _uow = uow;
        }

        public IQueryable<User> GetAll()
        {
            return _uow.Users.GetAll();
        }

        public IQueryable<User> GetUsers(int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords)
        {
            return _uow.Users.GetUsers(pageSize, pageNumber, sortField, asc, out totalRecords);
        }

        /// <summary>
        /// Возвращает пользователя со связанными таблицами
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public User GetById(int userId)
        {
            return _uow.Users.GetById(userId);
        }

        /// <summary>
        /// Возвращает пользователя со связанными таблицами
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public User GetByLogin(string login)
        {
            return _uow.Users.GetByLogin(login);
        }

        /// <summary>
        /// Возвращает пользователя без связанных таблиц
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public User GetUserInfoByLogin(string login)
        {
            return _uow.Users.GetUserInfoByLogin(login);
        }

        public void Create(User user)
        {
            _uow.Users.Add(user);

            // Установить свойства связанных бизнес-единиц в Unmodified,
            // чтобы они не добавлялись в базу
            foreach (var bu in user.BusinessUnits)
            {
                var buEntity = _uow.BusinessUnits.GetById(bu.BusinessUnitId);
                if (buEntity != null)
                {
                    // TODO: Исправить - вынесит из Service
                    _uow.BusinessUnits.SetUnchanged(buEntity);
                }
            }
            
            _uow.Commit();
        }

        public void Update(User user)
        {
            _uow.Users.Update(user);

            // Установить свойства связанных бизнес-единиц в Unmodified,
            // чтобы они не добавлялись в базу
            foreach (var bu in user.BusinessUnits)
            {
                var buEntity = _uow.BusinessUnits.GetById(bu.BusinessUnitId);
                if (buEntity != null)
                {
                    // TODO: Исправить - вынесит из Service
                    _uow.BusinessUnits.SetUnchanged(buEntity);
                }
            }

            

            _uow.Commit();
        }

        public void DeleteUser(int id)
        {
            _uow.Users.Delete(id);
            _uow.Commit();
        }
    }
}
