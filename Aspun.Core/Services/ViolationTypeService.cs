﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aspun.Common;
using Aspun.Core.Contracts;
using Aspun.Core.Model;

namespace Aspun.Core.Services
{
    public class ViolationTypeService
    {
        private IAspunUow _uow;

        public ViolationTypeService(IAspunUow uow)
        {
            _uow = uow;
        }

        public void UpdateList(List<ViolationType> newList)
        {
            _uow.ViolationTypes.UpdateAll(newList);
        }

        public IEnumerable<ViolationType> GetAll()
        {
            return _uow.ViolationTypes.GetAll();
        }

        public ViolationType GetById(int id)
        {
            return _uow.ViolationTypes.GetById(id);
        }
    }
}
