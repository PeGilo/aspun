﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aspun.Common;
using Aspun.Core.Contracts;
using Aspun.Core.Model;
using System.Data.SqlClient;
using System.Data;

namespace Aspun.Core.Services
{
    public class EmployeeService
    {
        private IAspunUow _uow;

        public EmployeeService(IAspunUow uow)
        {
            _uow = uow;
        }

        public Employee GetById(int id)
        {
            return _uow.Employees.GetById(id);
        }

        public IQueryable<Employee> GetEmployees(int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords)
        {
            return _uow.Employees.GetAllEmployees(pageSize, pageNumber, sortField, asc, out totalRecords);
        }

        public IQueryable<Employee> GetEmployees(IEnumerable<string> businessUnitName, int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords)
        {
            return _uow.Employees.GetEmployeesByBU(businessUnitName, pageSize, pageNumber, sortField, asc, out totalRecords);
        }

        public IQueryable<Employee> SearchEmployees(int maxCount, IList<string> fieldsToSearch, string searchTerm)
        {
            return _uow.Employees.SearchEmployees(maxCount, fieldsToSearch, searchTerm);
        }

        public IQueryable<Employee> SearchEmployees(IEnumerable<string> businessUnitName, int maxCount, IList<string> fieldsToSearch, string searchTerm)
        {
            return _uow.Employees.SearchEmployees(businessUnitName, maxCount, fieldsToSearch, searchTerm);
        }

        public Employee GetEmployeeBy(string number, string businessUnit)
        {
            return _uow.Employees.GetEmployeeBy(number, businessUnit);
        }

        /// <summary>
        /// Очищает таблицу сотрудников и заполняет ее переданными данными.
        /// </summary>
        /// <param name="employees">Коллекция массивов объектов.
        /// Каждый массив содержит аттрибуты Сотрудника. Индексы следующие:
        /// 0 - Табельный номер
        /// 1 - ФИО
        /// 2 - Должность
        /// 3 - Подразделение
        /// 4 - Бизнес единица
        /// </param>
        /// <returns>Кол-во импортированных сотрудников</returns>
        //public int ImportEmployees(IEnumerable<object[]> employees)
        //{
        //    int count = 0;

        //    _uow.Employees.RemoveAll();

        //    //_uow.Employees.RemoveAllAndImportEmployees();

        //    foreach (object[] e in employees)
        //    {
        //        Employee employee = new Employee()
        //        {
        //            Number = (e[0] != null) ? StringUtils.Substring((string)e[0], Employee.MAXL_Number) : String.Empty,
        //            FIO = (e[1] != null) ? StringUtils.Substring((string)e[1], Employee.MAXL_FIO) : String.Empty,
        //            Position = (e[2] != null) ? StringUtils.Substring((string)e[2], Employee.MAXL_Position) : String.Empty,
        //            Unit = (e[3] != null) ? StringUtils.Substring((string)e[3], Employee.MAXL_Unit) : String.Empty,
        //            BusinessUnit = (e[4] != null) ? StringUtils.Substring((string)e[4], Employee.MAXL_BusinessUnit) : String.Empty
        //        };

        //        if (!employee.IsEmpty())
        //        {
        //            _uow.Employees.Add(employee);
        //            count++;
        //        }
        //    }
        //    _uow.Commit();
        //    return count;
        //}

        public int ImportEmployees(string connectionString, IEnumerable<object[]> employees)
        {
            // TODO: Перенести в Repository
            _uow.Employees.RemoveAll();
            _uow.Commit();
            
            int count = 0;

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Number", typeof(String)));
            dt.Columns.Add(new DataColumn("FIO", typeof(String)));
            dt.Columns.Add(new DataColumn("Position", typeof(String)));
            dt.Columns.Add(new DataColumn("Unit", typeof(String)));
            dt.Columns.Add(new DataColumn("BusinessUnit", typeof(String)));

            foreach (object[] e in employees)
            {
                string number = (e[0] != null) ? StringUtils.Substring((string)e[0], Employee.MAXL_Number) : String.Empty;
                string fio = (e[1] != null) ? StringUtils.Substring((string)e[1], Employee.MAXL_FIO) : String.Empty;
                string position = (e[2] != null) ? StringUtils.Substring((string)e[2], Employee.MAXL_Position) : String.Empty;
                string unit = (e[3] != null) ? StringUtils.Substring((string)e[3], Employee.MAXL_Unit) : String.Empty;
                string businessUnit = (e[4] != null) ? StringUtils.Substring((string)e[4], Employee.MAXL_BusinessUnit) : String.Empty;

                if (!String.IsNullOrEmpty(number)
                    || !String.IsNullOrEmpty(fio)
                    || !String.IsNullOrEmpty(position)
                    || !String.IsNullOrEmpty(unit)
                    || !String.IsNullOrEmpty(businessUnit))
                {
                    dt.Rows.Add(new object[] { number, fio, position, unit, businessUnit });
                    count++;
                }
            }

            using (var connection = new SqlConnection(connectionString))
            {
                SqlTransaction transaction = null;
                connection.Open();
                try
                {
                    transaction = connection.BeginTransaction();
                    using (var sqlBulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.TableLock, transaction))
                    {
                        sqlBulkCopy.DestinationTableName = "Employee";
                        sqlBulkCopy.ColumnMappings.Add("Number", "Number");
                        sqlBulkCopy.ColumnMappings.Add("FIO", "FIO");
                        sqlBulkCopy.ColumnMappings.Add("Position", "Position");
                        sqlBulkCopy.ColumnMappings.Add("Unit", "Unit");
                        sqlBulkCopy.ColumnMappings.Add("BusinessUnit", "BusinessUnit");
                        sqlBulkCopy.WriteToServer(dt);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw;
                }

            }
            return count;
        }

        public void Create(Employee employee)
        {
            _uow.Employees.Add(employee);

            _uow.Commit();
        }

        public void Update(Employee employee)
        {
            _uow.Employees.Update(employee);

            _uow.Commit();
        }

        public void DeleteEmployee(int id)
        {
            _uow.Employees.Delete(id);
            _uow.Commit();
        }
    }
}
