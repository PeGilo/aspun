namespace Aspun.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Aspun.Core.Model;
    
    public partial class AddViolationEmployeeUnit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Violation", "EmployeeUnit", c => c.String(maxLength: Employee.MAXL_Unit));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Violation", "EmployeeUnit");
        }
    }
}
