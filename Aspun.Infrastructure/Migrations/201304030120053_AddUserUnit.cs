namespace Aspun.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Aspun.Core.Model;

    public partial class AddUserUnit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "Unit", c => c.String(maxLength: BusinessUnit.MAXL_NAME));
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "Unit");
        }
    }
}
