namespace Aspun.Infrastructure.Migrations
{
    using Aspun.Core.Model;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddViolationPrefix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Violation", "Prefix", c => c.String(maxLength: Violation.MAXL_PREFIX));
            AddColumn("dbo.Violation", "Index", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Violation", "Index");
            DropColumn("dbo.Violation", "Prefix");
        }
    }
}
