namespace Aspun.Infrastructure.Migrations
{
    using Aspun.Core.Model;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBusinessUnitPrefix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusinessUnit", "Prefix", c => c.String(maxLength: Violation.MAXL_PREFIX));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BusinessUnit", "Prefix");
        }
    }
}
