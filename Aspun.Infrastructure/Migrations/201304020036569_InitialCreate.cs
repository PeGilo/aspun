namespace Aspun.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        EmployeeId = c.Int(nullable: false, identity: true),
                        FIO = c.String(maxLength: 80),
                        Number = c.String(maxLength: 15),
                        Position = c.String(maxLength: 250),
                        Unit = c.String(maxLength: 250),
                        BusinessUnit = c.String(maxLength: 80),
                    })
                .PrimaryKey(t => t.EmployeeId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Login = c.String(maxLength: 80),
                        Name = c.String(maxLength: 80),
                        IsAllowed = c.Boolean(nullable: false),
                        PermissionSetSerialized = c.String(),
                        HasAccessToAllBusinessUnits = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.BusinessUnit",
                c => new
                    {
                        BusinessUnitId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.BusinessUnitId);
            
            CreateTable(
                "dbo.Violation",
                c => new
                    {
                        ViolationId = c.Int(nullable: false, identity: true),
                        Number = c.String(maxLength: 20),
                        Date = c.DateTime(nullable: false, storeType: "datetime2"),
                        BusinessUnitName = c.String(maxLength: 250),
                        EmployeeFIO = c.String(maxLength: 80),
                        EmployeeNumber = c.String(maxLength: 15),
                        EmployeePosition = c.String(maxLength: 250),
                        ViolationTypeDescription = c.String(maxLength: 250),
                        ViolationTypePar = c.String(maxLength: 80),
                        ViolationTypeCode = c.String(maxLength: 80),
                        Description = c.String(maxLength: 1024),
                        CreatorUserName = c.String(maxLength: 80),
                    })
                .PrimaryKey(t => t.ViolationId);
            
            CreateTable(
                "dbo.ViolationType",
                c => new
                    {
                        ViolationTypeId = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 250),
                        RegulationsPar = c.String(maxLength: 80),
                        Code = c.String(maxLength: 80),
                    })
                .PrimaryKey(t => t.ViolationTypeId);
            
            CreateTable(
                "dbo.UserBusinessUnit",
                c => new
                    {
                        User_UserId = c.Int(nullable: false),
                        BusinessUnit_BusinessUnitId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_UserId, t.BusinessUnit_BusinessUnitId })
                .ForeignKey("dbo.User", t => t.User_UserId, cascadeDelete: true)
                .ForeignKey("dbo.BusinessUnit", t => t.BusinessUnit_BusinessUnitId, cascadeDelete: true)
                .Index(t => t.User_UserId)
                .Index(t => t.BusinessUnit_BusinessUnitId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserBusinessUnit", new[] { "BusinessUnit_BusinessUnitId" });
            DropIndex("dbo.UserBusinessUnit", new[] { "User_UserId" });
            DropForeignKey("dbo.UserBusinessUnit", "BusinessUnit_BusinessUnitId", "dbo.BusinessUnit");
            DropForeignKey("dbo.UserBusinessUnit", "User_UserId", "dbo.User");
            DropTable("dbo.UserBusinessUnit");
            DropTable("dbo.ViolationType");
            DropTable("dbo.Violation");
            DropTable("dbo.BusinessUnit");
            DropTable("dbo.User");
            DropTable("dbo.Employee");
        }
    }
}
