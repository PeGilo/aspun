﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aspun.Common;
using Aspun.Core.Contracts;
using Aspun.Core.Model;
using System.Data.Entity.Infrastructure;
using System.Transactions;
using System.Data;

namespace Aspun.Infrastructure.Data
{
    public class ViolationRepository : EFRepository<Violation>, IViolationRepository
    {
        //private static readonly object _tableLocker = new Object();
        private IUnityLocker _unityLocker;

        public ViolationRepository(DbContext context, IUnityLocker unityLocker) 
            : base(context) 
        {
            _unityLocker = unityLocker;
        }

        /// <summary>
        /// Возвращает все нарушения
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortField"></param>
        /// <param name="asc"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public IQueryable<Violation> GetAllViolations(int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords)
        {
            return GetViolationsByBU(null, pageSize, pageNumber, sortField, asc, out totalRecords);
        }

        /// <summary>
        /// Возвращает нарушения отфильтрованные по названиям бизнес-единиц
        /// </summary>
        /// <param name="buNames">если null, то не фильтрует</param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortField"></param>
        /// <param name="asc"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public IQueryable<Violation> GetViolationsByBU(IEnumerable<string> buNames, int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords)
        {
            if (pageSize < 0)
            {
                pageSize = 0;
            }
            if (pageNumber < 1)
            {
                pageNumber = 1;
            }

            var q = GetAll();

            if (buNames != null)
            {
                q = q.Where(v => buNames.Contains(v.BusinessUnitName));
            }

            // подсчет кол-ва после фильтрации
            totalRecords = q.Count();

            if (!String.IsNullOrEmpty(sortField))
            {
                string orderClause = sortField + " " + (asc.HasValue ? (asc.Value ? "ASC" : "DESC") : "ASC");
                q = q.OrderBy(orderClause);
            }
            else
            {
                q = q.OrderByDescending(u => u.Date); // default sort
            }


            return q.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public override void Add(Violation entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                Insert(entity);

                //DbSet.Add(entity);
            }
        }

        protected virtual void Insert(Violation entity)
        {
            // Исключить вставку других нарушений на время вставки,
            // чтобы не нарушить нумерацию
            //lock (_tableLocker)
            //{
            //    // Использован режим Serializable, чтобы заблокировать таблицы от изменений (от вставки строк)
            //    using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required,
            //                                    new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.Serializable }))
            //    {

            //_lockManager.AcquireLock("ViolationRepository", LockResource.Violation);
            _unityLocker.AcquireLock("ViolationRepository", LockResource.Violation);

            try
            {
                // 1. Определить максимальный индекс для заданного префикса

                int index = (from v in DbSet
                             where v.Prefix == entity.Prefix
                             select v.Index).DefaultIfEmpty(0).Max();
                //var res = (from v in DbSet
                //               select v).Take(1);

                //int index = DbSet.Count(v => v.Prefix == entity.Prefix);

                // Изменяет entity, так что в вызывающем коде тоже можно будет узнать новый номер
                entity.Index = index + 1;
                //DbContext.SaveChanges();

                // 2. Добавить непосредственного запись

                DbSet.Add(entity);

                //DbContext.SaveChanges();


                //        transaction.Complete();
                //    }
                //}

            }
            catch(Exception ex)
            {
                _unityLocker.ReleaseLock("ViolationRepository", LockResource.Violation);
            }
        }
    }
}
