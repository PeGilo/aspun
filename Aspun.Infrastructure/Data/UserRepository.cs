﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aspun.Common;
using Aspun.Core.Contracts;
using Aspun.Core.Model;

namespace Aspun.Infrastructure.Data
{
    public class UserRepository : EFRepository<User>, IUserRepository
    {
        public UserRepository(DbContext context, IUnityLocker unityLocker)
            : base(context) 
        {
        }

        public override IQueryable<User> GetAll()
        {
            return base.GetAll();
        }

        public IQueryable<User> GetUsers(int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords)
        {
            if (pageSize < 0)
            {
                pageSize = 0;
            }
            if (pageNumber < 1)
            {
                pageNumber = 1;
            }

            //SetLoadOptions();
            var q = GetAll();// _dataContext.Inspection.Where(i => i.AuditorID == auditorId);
            totalRecords = q.Count();

            if (!String.IsNullOrEmpty(sortField))
            {
                string orderClause = sortField + " " + (asc.HasValue ? (asc.Value ? "ASC" : "DESC") : "ASC");
                q = q.OrderBy(orderClause);
            }
            else
            {
                q = q.OrderBy(u => u.Login); // default sort
            }
            return q.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public override void Add(User entity)
        {
            SerializeFields(entity);
            base.Add(entity);
        }

        public override User GetById(int id)
        {
            User entity = base.GetById(id);

            if (entity != null)
            {
                DbContext.Entry(entity).Collection(u => u.BusinessUnits).Load();

                DeserializeFields(entity);
            }

            return entity;
        }

        /// <summary>
        /// Со связанными таблицами
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public virtual User GetByLogin(string login)
        {
            User entity = (from u in DbSet
                           where u.Login == login
                           select u).FirstOrDefault();

            if (entity != null)
            {
                DbContext.Entry(entity).Collection(u => u.BusinessUnits).Load();

                DeserializeFields(entity);
            }

            return entity;
        }

        /// <summary>
        /// Без связанных таблиц
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public virtual User GetUserInfoByLogin(string login)
        {
            User entity = (from u in DbSet
                           where u.Login == login
                           select u).FirstOrDefault();

            if (entity != null)
            {
                DeserializeFields(entity);
            }

            return entity;
        }

        public override void Update(User entity)
        {
            SerializeFields(entity);
            base.Update(entity);
        }
        
        private static void SerializeFields(User entity)
        {
            entity.PermissionSetSerialized = SerializationHelper.Serialize<PermissionSet>(entity.PermissionSet);
        }

        private static void DeserializeFields(User user)
        {
            if (user != null && !String.IsNullOrEmpty(user.PermissionSetSerialized))
            {
                user.PermissionSet = SerializationHelper.Deserialize<PermissionSet>(user.PermissionSetSerialized);
            }
        }
    }
}
