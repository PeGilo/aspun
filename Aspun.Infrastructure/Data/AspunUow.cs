﻿using Aspun.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Infrastructure.Data
{
    public interface IUnityLocker
    {
        void AcquireLock(string owner, LockResource resource);
        void ReleaseLock(string owner, LockResource resource);
    }

    /// <summary>
    /// The "Unit of Work"
    ///     1) decouples the repos from the controllers
    ///     2) decouples the DbContext and EF from the controllers
    ///     3) manages the UoW
    /// </summary>
    /// <remarks>
    /// This class implements the "Unit of Work" pattern in which
    /// the "UoW" serves as a facade for querying and saving to the database.
    /// Querying is delegated to "repositories".
    /// Each repository serves as a container dedicated to a particular
    /// root entity type such as a <see cref="Transport"/>.
    /// A repository typically exposes "Get" methods for querying and
    /// will offer add, update, and delete methods if those features are supported.
    /// The repositories rely on their parent UoW to provide the interface to the
    /// data layer (which is the EF DbContext).
    /// </remarks>
    public class AspunUow : IAspunUow, IUnityLocker, IDisposable
    {
        private LockManager _lockManager;
        private List<Tuple<string, LockResource>> _acquiredLocks;

        //private Dictionary<string

        public AspunUow(IRepositoryProvider repositoryProvider, LockManager lockManager)
        {
            _lockManager = lockManager;
            _acquiredLocks = new List<Tuple<string, LockResource>>();

            CreateDbContext();

            repositoryProvider.DbContext = DbContext;
            repositoryProvider.UnityLocker = this;
            RepositoryProvider = repositoryProvider;
        }

        // Repositories

        public IEmployeeRepository Employees { get { return GetRepo<IEmployeeRepository>(); } }
        public IUserRepository Users { get { return GetRepo<IUserRepository>(); } }
        public IViolationRepository Violations { get { return GetRepo<IViolationRepository>(); } }
        public IViolationTypeRepository ViolationTypes { get { return GetRepo<IViolationTypeRepository>(); } }
        public IBusinessUnitRepository BusinessUnits { get { return GetRepo<IBusinessUnitRepository>(); } }

        /// <summary>
        /// Save pending changes to the database
        /// </summary>
        public void Commit()
        {
            try
            {
                DbContext.SaveChanges();
            }
            finally
            {
                ReleaseAllLocks();
            }
        }

        public void Rollback()
        {
            ReleaseAllLocks();
        }

        protected void CreateDbContext()
        {
            DbContext = new AspunDbContext();
            
            // Do NOT enable proxied entities, else serialization fails
            DbContext.Configuration.ProxyCreationEnabled = false;

            // Load navigation properties explicitly (avoid serialization trouble)
            DbContext.Configuration.LazyLoadingEnabled = false;

            // Because Web API will perform validation, we don't need/want EF to do so
            DbContext.Configuration.ValidateOnSaveEnabled = false;

            //DbContext.Configuration.AutoDetectChangesEnabled = false;
            // We won't use this performance tweak because we don't need 
            // the extra performance and, when autodetect is false,
            // we'd have to be careful. We're not being that careful.
        }

        protected IRepositoryProvider RepositoryProvider { get; set; }

        //private IRepository<T> GetStandardRepo<T>() where T : class
        //{
        //    return RepositoryProvider.GetRepositoryForEntityType<T>();
        //}
        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }

        private AspunDbContext DbContext { get; set; }

        #region IUnityLocker

        public void AcquireLock(string owner, LockResource resource)
        {
            _lockManager.AcquireLock(owner, resource);
            _acquiredLocks.Add(new Tuple<string, LockResource>(owner, resource));
        }

        public void ReleaseLock(string owner, LockResource resource)
        {
            int index = _acquiredLocks.FindIndex(
                tu => tu.Item1 == owner && tu.Item2 == resource);

            if (index >= 0)
            {
                _acquiredLocks.RemoveAt(index);
            }

            _lockManager.ReleaseLock(owner, resource);

        }

        private void ReleaseAllLocks()
        {
            foreach (var lck in _acquiredLocks)
            {
                _lockManager.ReleaseLock(lck.Item1, lck.Item2);
            }
            _acquiredLocks.Clear();
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                }
            }
        }

        #endregion
    }
}
