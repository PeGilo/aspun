﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

using Aspun.Common;
using Aspun.Core.Contracts;
using Aspun.Core.Model;

namespace Aspun.Infrastructure.Data
{
    public class EmployeeRepository : EFRepository<Employee>, IEmployeeRepository
    {
        private static readonly object _tableLocker = new Object();

        public EmployeeRepository(DbContext context, IUnityLocker unityLocker) : base(context) { }

        public Employee GetEmployeeBy(string number, string businessUnit)
        {
            Employee entity = (from e in DbSet
                                where e.Number == number && e.BusinessUnit == businessUnit
                                select e).FirstOrDefault();

            return entity;
        }

        /// <summary>
        /// Возвращает всех сотрудников
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortField"></param>
        /// <param name="asc"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public IQueryable<Employee> GetAllEmployees(int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords)
        {
            return GetEmployeesByBU(null, pageSize, pageNumber, sortField, asc, out totalRecords);
        }

        /// <summary>
        /// Возвращает сотрудников отфильтрованных по названиям бизнес-единиц
        /// </summary>
        /// <param name="buNames">если null, то не фильтрует</param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortField"></param>
        /// <param name="asc"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public IQueryable<Employee> GetEmployeesByBU(IEnumerable<string> buNames, int pageSize, int pageNumber, string sortField, bool? asc, out int totalRecords)
        {
            if (pageSize < 0)
            {
                pageSize = 0;
            }
            if (pageNumber < 1)
            {
                pageNumber = 1;
            }

            // SetLoadOptions();
            var q = GetAll();// _dataContext.Inspection.Where(i => i.AuditorID == auditorId);
            
            if (buNames != null)
            {
                q = q.Where(emp => buNames.Contains(emp.BusinessUnit));
            }

            // подсчет кол-ва после фильтрации
            totalRecords = q.Count();

            if (!String.IsNullOrEmpty(sortField))
            {
                string orderClause = sortField + " " + (asc.HasValue ? (asc.Value ? "ASC" : "DESC") : "ASC");
                q = q.OrderBy(orderClause);
            }
            else
            {
                q = q.OrderBy(u => u.FIO); // default sort
            }


            return q.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public IQueryable<Employee> SearchEmployees(int maxCount, IList<string> fieldsToSearch, string searchTerm)
        {
            return SearchEmployees(null, maxCount, fieldsToSearch, searchTerm);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="businessUnitName">if null, selects for all business </param>
        /// <param name="maxCount"></param>
        /// <param name="fieldsToSearch"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        public IQueryable<Employee> SearchEmployees(IEnumerable<string> buNames, int maxCount, IList<string> fieldsToSearch, string searchTerm)
        {
            var q = GetAll();

            if (buNames != null)
            {
                q = q.Where(emp => buNames.Contains(emp.BusinessUnit));
            }

            if (fieldsToSearch != null && !String.IsNullOrEmpty(searchTerm))
            {
                //StringBuilder sb = new StringBuilder();
                
                //// fild LIKE '%term%'
                //for(int i = 0; i < fieldsToSearch.Count; i++)
                //{
                //    sb.Append(fieldsToSearch[i] + " LIKE '%" + searchTerm + "%'");
                //    if(i < fieldsToSearch.Count - 1)
                //    {
                //        sb.Append(" OR ");
                //    }
                //}

                //q = q.Where(sb.ToString());
                q = q.Where(emp => emp.FIO.Contains(searchTerm) || emp.Number.Contains(searchTerm) || emp.Position.Contains(searchTerm));
            }

            return q.OrderBy("FIO").Take(maxCount);
        }

        public void RemoveAll()
        {
            // Исключить одновременную работу со строкам в таблице во время генерации отчета
            lock (_tableLocker)
            {
                // Использован режим Serializable, чтобы заблокировать таблицы от изменений
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required,
                                                new TransactionOptions() { IsolationLevel = IsolationLevel.Serializable }))
                {
                    // Remove all
                    foreach (Employee e in DbSet)
                    {
                        DbSet.Remove(e);
                    }
                    DbContext.SaveChanges();

                    transaction.Complete();
                }
            }
        }


        //public void RemoveAllAndImportEmployees(IEnumerable<Employee> employees)
        //{
        //    // Исключить одновременную работу со строкам в таблице во время генерации отчета
        //    lock (_tableLocker)
        //    {
        //        // Использован режим Serializable, чтобы заблокировать таблицы от изменений
        //        using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required,
        //                                        new TransactionOptions() { IsolationLevel = IsolationLevel.Serializable }))
        //        {
        //            // Remove all
        //            foreach (Employee e in DbSet)
        //            {
        //                DbSet.Remove(e);
        //            }
        //            // DbContext.SaveChanges(); ??

        //            foreach(Employee e in employees)
        //            {
        //                DbSet.Add(e);
        //            }

        //            transaction.Complete();
        //        }
        //    }
        //}
    }
}
