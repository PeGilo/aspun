﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aspun.Common;
using Aspun.Core.Contracts;
using Aspun.Core.Model;
using System.Transactions;

namespace Aspun.Infrastructure.Data
{
    public class ViolationTypeRepository : EFRepository<ViolationType>, IViolationTypeRepository
    {
        private static readonly object _tableLocker = new Object();

        public ViolationTypeRepository(DbContext context, IUnityLocker unityLocker) : base(context) { }

        public void UpdateAll(IList<ViolationType> newList)
        {
            // Исключить одновременную работу со строкам в таблице во время генерации отчета
            lock (_tableLocker)
            {
                // Использован режим Serializable, чтобы заблокировать таблицы от изменений
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required,
                                                new TransactionOptions() { IsolationLevel = IsolationLevel.Serializable }))
                {
                    IEnumerable<ViolationType> vtypes = GetAll();

                    foreach (ViolationType vtype in vtypes)
                    {
                        // найти в новом списке,
                        ViolationType newVType = newList.Where(vt => vt.ViolationTypeId == vtype.ViolationTypeId).FirstOrDefault();
                        // если найдено, значит Update
                        if (newVType != null)
                        {
                            vtype.Description = newVType.Description;
                            vtype.RegulationsPar = newVType.RegulationsPar;
                            vtype.Code = newVType.Code;
                        }
                        // если не найдено, значит Delete
                        else
                        {
                            Delete(vtype);
                        }
                    }

                    // Выбрать из нового списка все с Id = 0 и добавить их
                    foreach (ViolationType newVtype in newList.Where(vt => vt.ViolationTypeId == 0))
                    {
                        Add(newVtype);
                    }

                    DbContext.SaveChanges();

                    transaction.Complete();
                }
            }
        }
    }
}
