﻿using Aspun.Core.Model;
using Aspun.Infrastucture.SampleData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aspun.Infrastructure.Data
{
    public class AspunDbContext : DbContext
    {
        public AspunDbContext()
            :base(nameOrConnectionString: "DefaultConnection")
        {  
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Violation> Violations { get; set; }
        public DbSet<ViolationType> ViolationTypes { get; set; }
        public DbSet<BusinessUnit> BusinessUnits { get; set; }

        static AspunDbContext()
        {
            // Во время разработки база должна быть проинициализирована тестовыми данными,
            // а также удалена и создана заново, если схема изменилась.
            //Database.SetInitializer(new AspunDatabaseInitializer());

            // Убираем проверку схемы базы данных, так как использоуем EF Code First Migration
            Database.SetInitializer<AspunDbContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Не делать названия таблиц во множественном числе
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // Не записывать сложное поле в базу, оно будет сериализовано
            modelBuilder.Entity<User>().Ignore(u => u.PermissionSet);
            
            // Проинициализировать отношения между таблицами

            modelBuilder.Entity<User>()
                .HasMany(u => u.BusinessUnits)
                .WithMany();


            // Чтобы обычный DateTime вмещался в SQLDateTime
            modelBuilder.Entity<Violation>().Property(vt => vt.Date).HasColumnType("datetime2");
        }
    }
}
