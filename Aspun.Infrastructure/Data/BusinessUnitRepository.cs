﻿using Aspun.Core.Contracts;
using Aspun.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Aspun.Infrastructure.Data
{
    public class BusinessUnitRepository : EFRepository<BusinessUnit>, IBusinessUnitRepository
    {
        private static readonly object _tableLocker = new Object();

        public BusinessUnitRepository(DbContext context, IUnityLocker unityLocker) : base(context) { }

        /// <summary>
        /// Возвращает только не удаленные элементы
        /// </summary>
        /// <returns></returns>
        public IQueryable<BusinessUnit> GetAllNotDeleted()
        {
            return GetAll().Where(bu => bu.IsDeleted == false);
        }

        //public virtual void Update(T entity)
        //{
        //    DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
        //    if (dbEntityEntry.State == EntityState.Detached)
        //    {
        //        DbSet.Attach(entity);
        //    }

        //    dbEntityEntry.State = EntityState.Modified;
        //}

        //public virtual void Delete(T entity)
        //{
        //    DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
        //    if (dbEntityEntry.State != EntityState.Deleted)
        //    {
        //        dbEntityEntry.State = EntityState.Deleted;
        //    }
        //    else
        //    {
        //        DbSet.Attach(entity);
        //        DbSet.Remove(entity);
        //    }
        //}

        /// <summary>
        /// Помечает объект как удаленный, но не удаляет запись из базы
        /// </summary>
        /// <param name="entity"></param>
        public override void Delete(BusinessUnit entity)
        {
            // Пока решено отказаться от обозначения удаленных БЕ специальным полем
            // и удалять непосредственно записи из базы данных.
            //entity.IsDeleted = true;

            base.Delete(entity);
        }

        /// <summary>
        /// Изменяет записи в базе согласно переданному списку. Не затрагивает помеченные удаленными записи.
        /// </summary>
        /// <param name="newList"></param>
        public void UpdateAll(IList<BusinessUnit> newList)
        {
            // Исключить одновременную работу со строкам в таблице во время генерации отчета
            lock (_tableLocker)
            {
                // Использован режим Serializable, чтобы заблокировать таблицы от изменений
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required,
                                                new TransactionOptions() { IsolationLevel = IsolationLevel.Serializable }))
                {
                    IEnumerable<BusinessUnit> bunits = GetAllNotDeleted(); // Удаленные элементы не трогать

                    foreach (BusinessUnit bUnit in bunits)
                    {
                        // найти в новом списке,
                        BusinessUnit newBUnit = newList.Where(bu => bu.BusinessUnitId == bUnit.BusinessUnitId).FirstOrDefault();
                        // если найдено, значит Update
                        if (newBUnit != null)
                        {
                            bUnit.Name = newBUnit.Name;
                            bUnit.Prefix = newBUnit.Prefix;
                        }
                        // если не найдено, значит Delete
                        else
                        {
                            Delete(bUnit);
                        }
                    }

                    // Выбрать из нового списка все с Id = 0 и добавить их
                    foreach (BusinessUnit newBUnit in newList.Where(bu => bu.BusinessUnitId == 0))
                    {
                        Add(newBUnit);
                    }

                    DbContext.SaveChanges();

                    transaction.Complete();
                }
            }
        }
    }
}
