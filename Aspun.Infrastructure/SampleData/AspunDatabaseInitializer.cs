﻿using Aspun.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aspun.Core.Model;

namespace Aspun.Infrastucture.SampleData
{
    /// <summary>
    /// Инициализирует первоначальное состояние базы. Не используются при миграции базы.
    /// </summary>
    public class AspunDatabaseInitializer
        //: DropCreateDatabaseAlways<AspunDbContext>
        : CreateDatabaseIfNotExists<AspunDbContext>      // when model is stable
        //: DropCreateDatabaseIfModelChanges<AspunDbContext> // when iterating
    {
        protected override void Seed(AspunDbContext context)
        {
            // Добавить ограничение уникальность логинов пользователей
            context.Database.ExecuteSqlCommand("ALTER TABLE [User] ADD CONSTRAINT UNQ_UserLogin UNIQUE (Login)");

            // Закоммен-но потому что в экселевском файле не соблюдается уникальность
            //// Добавить ограничение уникальност (таб.номер, бизнес-единица) для сотрудника
            //context.Database.ExecuteSqlCommand("ALTER TABLE [Employee] ADD CONSTRAINT UNQ_EmployeeNumberBU UNIQUE (Number, BusinessUnit)");

            context.Users.Add(new User() {
                Login = @"sibgenco\GilayanPS", 
                Name = "Петр",
                IsAllowed = true,
                PermissionSetSerialized = "<?xml version=\"1.0\" encoding=\"utf-16\"?>"
                    + "<PermissionSet xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                    + "<UserManagementPermission>All</UserManagementPermission>"
                    + "<ViolationManagementPermission>All</ViolationManagementPermission>"
                    + "<ViolationTypeManagementPermission>All</ViolationTypeManagementPermission>"
                    + "<BusinessUnitManagementPermission>All</BusinessUnitManagementPermission>"
                    + "<EmployeeManagementPermission>InsertUpdateDeleteImport</EmployeeManagementPermission>"
                    + "<HistoryPermission>View</HistoryPermission>"
                    + "</PermissionSet>",
                HasAccessToAllBusinessUnits = true
            });

            context.Users.Add(new User()
            {
                Login = @"sibgenco\KuzinIN",
                Name = "Игорь",
                IsAllowed = true,
                PermissionSetSerialized = "<?xml version=\"1.0\" encoding=\"utf-16\"?>"
                    + "<PermissionSet xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                    + "<UserManagementPermission>All</UserManagementPermission>"
                    + "<ViolationManagementPermission>All</ViolationManagementPermission>"
                    + "<ViolationTypeManagementPermission>All</ViolationTypeManagementPermission>"
                    + "<BusinessUnitManagementPermission>All</BusinessUnitManagementPermission>"
                    + "<EmployeeManagementPermission>InsertUpdateDeleteImport</EmployeeManagementPermission>"
                    + "<HistoryPermission>View</HistoryPermission>"
                    + "</PermissionSet>",
                HasAccessToAllBusinessUnits = true
            });

            context.Users.Add(new User()
            {
                Login = @"sibgenco\RomanchukSI",
                Name = "Стас",
                IsAllowed = true,
                PermissionSetSerialized = "<?xml version=\"1.0\" encoding=\"utf-16\"?>"
                    + "<PermissionSet xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                    + "<UserManagementPermission>All</UserManagementPermission>"
                    + "<ViolationManagementPermission>All</ViolationManagementPermission>"
                    + "<ViolationTypeManagementPermission>All</ViolationTypeManagementPermission>"
                    + "<BusinessUnitManagementPermission>All</BusinessUnitManagementPermission>"
                    + "<EmployeeManagementPermission>InsertUpdateDeleteImport</EmployeeManagementPermission>"
                    + "<HistoryPermission>View</HistoryPermission>"
                    + "</PermissionSet>",
                HasAccessToAllBusinessUnits = true
            });

            context.Users.Add(new User()
            {
                Login = @"sibgenco\VergeychikAV",
                Name = "Алексей",
                IsAllowed = true,
                PermissionSetSerialized = "<?xml version=\"1.0\" encoding=\"utf-16\"?>"
                    + "<PermissionSet xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                    + "<UserManagementPermission>All</UserManagementPermission>"
                    + "<ViolationManagementPermission>All</ViolationManagementPermission>"
                    + "<ViolationTypeManagementPermission>All</ViolationTypeManagementPermission>"
                    + "<BusinessUnitManagementPermission>All</BusinessUnitManagementPermission>"
                    + "<EmployeeManagementPermission>InsertUpdateDeleteImport</EmployeeManagementPermission>"
                    + "<HistoryPermission>View</HistoryPermission>"
                    + "</PermissionSet>",
                HasAccessToAllBusinessUnits = true
            });

            //context.Users.Add(new User()
            //{
            //    Login = @"COREI5\Admin",
            //    Name = "Петр",
            //    IsAllowed = true,
            //    PermissionSetSerialized = "<?xml version=\"1.0\" encoding=\"utf-16\"?>"
            //        + "<PermissionSet xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
            //        + "<UserManagementPermission>All</UserManagementPermission>"
            //        + "<ViolationManagementPermission>All</ViolationManagementPermission>"
            //        + "<ViolationTypeManagementPermission>All</ViolationTypeManagementPermission>"
            //        + "<BusinessUnitManagementPermission>All</BusinessUnitManagementPermission>"
            //        + "<EmployeeManagementPermission>InsertUpdateDeleteImport</EmployeeManagementPermission>"
            //        + "<HistoryPermission>View</HistoryPermission>"
            //        + "</PermissionSet>",
            //    HasAccessToAllBusinessUnits = true
            //});

            var businessUnits = new List<BusinessUnit>()
            {
                new BusinessUnit() { Name = "Абаканская ТЭЦ" },
                new BusinessUnit() { Name = "Красноярская ТЭЦ-1" },
                new BusinessUnit() { Name = "Красноярская ТЭЦ-2" },
                new BusinessUnit() { Name = "Красноярская ТЭЦ-3" },
                new BusinessUnit() { Name = "Минусинская ТЭЦ" },
                new BusinessUnit() { Name = "ОАО  \"Канская ТЭЦ\"" },
                new BusinessUnit() { Name = "ОАО \"Дивногорские тепловые сети\"" },
                new BusinessUnit() { Name = "ОАО \"Красноярская теплотранспортная компания\"" },
                new BusinessUnit() { Name = "ОАО \"Красноярская ТЭЦ-1\"" },
                new BusinessUnit() { Name = "ОАО \"Красноярская ТЭЦ-4\"" },
                new BusinessUnit() { Name = "ОАО \"Красноярская электрокотельная\"" },
                new BusinessUnit() { Name = "ОАО \"Назаровская ГРЭС\"" },
                new BusinessUnit() { Name = "ОАО \"Южно-Енисейские тепловые сети\"" },
                new BusinessUnit() { Name = "ОАО Енисейская ТГК (ТГК-13)" },
                new BusinessUnit() { Name = "ООО \"Сибирская генерирующая компания\"" },
                new BusinessUnit() { Name = "Теплосеть" }
            };

            businessUnits.ForEach(bu => context.BusinessUnits.Add(bu));

            var violationTypes = new List<ViolationType>()
            {
                new ViolationType() { Description = "Курение в неположеном месте", RegulationsPar = "пар. 1 п. 2", Code = "Код А" },
                new ViolationType() { Description = "Несоблюдение правил ГГТН при использовании кранов, вышек, буроямов (неправильная установка, применение неиспытанных механизмов, тары, непригодных стропов и т.п.)", RegulationsPar = "пар. 2 п. 3", Code = "Код Б" },
                new ViolationType() { Description = "Нарушение требований ТБ при строповке, перемещении и складировании грузов", RegulationsPar = "пар. 5 п. 4", Code = "Код В" },
                new ViolationType() { Description = "- не установление переносных  заземления (не включение заземляющих ножей)", RegulationsPar = "пар. 7 п. 1", Code = "Код Г" }
            };

#if DEBUG
            violationTypes.ForEach(vt => context.ViolationTypes.Add(vt));
#endif

            var violations = new List<Violation>()
            {
                new Violation() { 
                    //Number="00001", 
                    Date = new DateTime(2013, 01, 9), 
                    BusinessUnitName = "Теплосеть",
                    EmployeeFIO = "Иванов Иван Иванович",
                    EmployeeNumber = "0923",
                    EmployeePosition = "Слесарь",
                    ViolationTypeDescription = "Алкогольное опьянение на рабочем месте",
                    ViolationTypePar = "пар. 5.1",
                    Description = "Пьянствовал на работе.",
                    CreatorUserName = "Петров Петр Петрович" },

                new Violation() { 
                    //Number="00002", 
                    Date = new DateTime(2013, 01, 10), 
                    BusinessUnitName = "Теплосеть",
                    EmployeeFIO = "Иванов Иван Иванович",
                    EmployeeNumber = "0923",
                    EmployeePosition = "Слесарь",
                    ViolationTypeDescription = "Курение в неположеном месте",
                    ViolationTypePar = "пар. 3.2",
                    Description = "Курил на рабочем месте.",
                    CreatorUserName = "Петров Петр Петрович" },

                new Violation() { 
                    //Number="00003", 
                    Date = new DateTime(2013, 02, 9), 
                    BusinessUnitName = "Абаканская ТЭЦ",
                    EmployeeFIO = "Сидоров Сидор",
                    EmployeeNumber = "0001",
                    EmployeePosition = "Слесарь",
                    ViolationTypeDescription = "Алкогольное опьянение на рабочем месте",
                    ViolationTypePar = "пар. 5.1",
                    Description = "Пьянствовал на работе.",
                    CreatorUserName = "Петров Петр Петрович" },

                new Violation() { 
                    //Number="00004", 
                    Date = new DateTime(2013, 02, 9), 
                    BusinessUnitName = "Абаканская ТЭЦ",
                    EmployeeFIO = "Сидоров Сидор",
                    EmployeeNumber = "0001",
                    EmployeePosition = "Слесарь",
                    ViolationTypeDescription = "Курение в неположеном месте",
                    ViolationTypePar = "пар. 3.2",
                    Description = "Курил на рабочем месте.",
                    CreatorUserName = "Романов Роман" },

                new Violation() { 
                    //Number="00005", 
                    Date = new DateTime(2013, 03, 10), 
                    BusinessUnitName = "Теплосеть",
                    EmployeeFIO = "Забегайло Зохан",
                    EmployeeNumber = "0005",
                    EmployeePosition = "Слесарь",
                    ViolationTypeDescription = "Работа без каски",
                    ViolationTypePar = "пар. 10.1",
                    Description = "Работал без каски.",
                    CreatorUserName = "Смирнов А.П." },

                new Violation() { 
                    //Number="00006", 
                    Date = new DateTime(2013, 03, 10), 
                    BusinessUnitName = "Теплосеть",
                    EmployeeFIO = "Жирнов Житомир",
                    EmployeeNumber = "0006",
                    EmployeePosition = "Монтажник",
                    ViolationTypeDescription = "Работа без каски",
                    ViolationTypePar = "пар. 10.1",
                    Description = "Работал без каски.",
                    CreatorUserName = "Смирнов А.П." },

                new Violation() { 
                    //Number="00007", 
                    Date = new DateTime(2013, 03, 11), 
                    BusinessUnitName = "Абаканская ТЭЦ",
                    EmployeeFIO = "Кузнецов Николай",
                    EmployeeNumber = "0008",
                    EmployeePosition = "Бригадир",
                    ViolationTypeDescription = "Работа без каски",
                    ViolationTypePar = "пар. 10.1",
                    Description = "Работал без каски.",
                    CreatorUserName = "Глазунов Георгий" }
            };

#if DEBUG
            violations.ForEach(vt => context.Violations.Add(vt));
#endif

            var employees = new List<Employee>()
            {
                new Employee() { FIO="Иванов Иван Иванович", Number="0001", Position="Слесарь", Unit="Подразделение 1", BusinessUnit="Теплосеть" },
                new Employee() { FIO="Петров Петр Петрович", Number="0002", Position="Монтажник", Unit="Подразделение 2", BusinessUnit="Теплосеть" },
                new Employee() { FIO="Сидорово Сидор Сидорович", Number="0003", Position="Бригадир", Unit="Подразделение 3", BusinessUnit="Теплосеть" },
                new Employee() { FIO="Романов Роман Романович", Number="0001", Position="Слесарь", Unit="Подразделение 1", BusinessUnit="Абаканская ТЭЦ" },
                new Employee() { FIO="Сергеев Сергей Сергеевич", Number="0002", Position="Монтажник", Unit="Подразделение 2", BusinessUnit="Абаканская ТЭЦ" },
            };

#if DEBUG
            employees.ForEach(e => context.Employees.Add(e));
#endif
            
            context.SaveChanges();
        }
    }
}
