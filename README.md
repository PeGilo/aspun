# Registring discipline violations #
This web-application is used for managing discipline violation records with descrete access. 

This is an ASP.NET MVC app.

Used "Onion architecture" with core entities separated from infrastructure and presentation logic.
Repository pattern is utilized for separating concerns along with Unit of Work pattern. Dependencies are handled with Ninject. Database access is built using Code-First approach.

Developer environment: **C# (.NET Framework 3.5), Visual Studio**.

Used:

 * **ASP.NET MVC 3**
 * **Entity Framework 5 (Code-First)** - for object-relation mapping;
 * **Ninject** - for handling dependency injection;
 * **log4net** - for logging;
 * Visual Studio **Unit Testing Framework**.
 * SQL Server 2005