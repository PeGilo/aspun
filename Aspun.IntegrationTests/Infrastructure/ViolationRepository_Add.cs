﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Aspun.Infrastructure.Data;
using Aspun.Core.Model;
using System.Threading.Tasks;
using System.Threading;
using Aspun.Infrastructure;
using System.Diagnostics;

namespace Aspun.IntegrationTests.Infrastructure
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class ViolationRepository_Add
    {
        public ViolationRepository_Add()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            TextWriterTraceListener writer = new TextWriterTraceListener(System.Console.Out);
            Debug.Listeners.Add(writer);        
        }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private struct TaskState
        {
            public LockManager lm;
            public string prefix;
        }

        //[TestMethod]
        //public void ParallelInsertAndDelete()
        //{
        //    var context = new AspunDbContext();
        //    context.Database.ExecuteSqlCommand("truncate table Violation");
        //    var repository = new ViolationRepository(context);

        //    for (int i = 0; i < 50; i++)
        //    {
        //        // Вставить одну запись
        //        InsertViolationTask(new TaskState() { prefix = "XXX" });

        //        // Одновременно удалить последнюю запись и вставить еще одну.
        //        Task[] tasks = new Task[2]{
        //            new Task(InsertViolationTask, new TaskState() { prefix = "XXX" })
        //        };
        //    }
        //}

        /// <summary>
        /// Тестирование параллельной вставки, номера не должны повторяться
        /// </summary>
        [TestMethod]
        public void ParallelInsert()
        {
            var context = new AspunDbContext();
            context.Database.ExecuteSqlCommand("truncate table Violation");

            LockManager lm = new LockManager();
            RepositoryFactories rf = new RepositoryFactories();
            RepositoryProvider repositoryProvider = new RepositoryProvider(rf);
            var uow = new AspunUow(repositoryProvider, lm);

            var repository = new ViolationRepository(context, uow);

            //Task.Factory.StartNew(new Action<object>(InsertViolationTask), "");

            string prefixA = "AAA", prefixB = "BBB";
            int tasksCount = 100;
            Task[] tasks = new Task[tasksCount];

            for (int i = 0; i < tasksCount; i++)
            {
                tasks[i] = new Task(InsertViolationTask, 
                    new TaskState() { 
                        prefix = ((i % 2) == 0 ? prefixA : prefixB),
                        lm = lm // LockManager должен быть общим для всех задач
                    });
            }

            for (int i = 0; i < tasksCount; i++)
            {
                tasks[i].Start();
            }
            Task.WaitAll(tasks);


            // Проверка результата
            int count = (from v in context.Violations
                         select v).Count();

            // ... Проверить сколько всего получилось в базе записей
            //     - каждый поток должен создат одну запись
            Assert.AreEqual(tasksCount, count);

            int maxNumberA = (from v in context.Violations
                              where v.Prefix == prefixA
                              select v.Index).DefaultIfEmpty(0).Max();

            int maxNumberB = (from v in context.Violations
                              where v.Prefix == prefixB
                              select v.Index).DefaultIfEmpty(0).Max();

            Assert.AreEqual(tasksCount / 2, maxNumberA, "Все номера должны быть уникальными, от 1 до 25");
            Assert.AreEqual(tasksCount / 2, maxNumberB, "Все номера должны быть уникальными, от 1 до 25");

            //foreach (var v in context.Violations)
            //{
            //    context.Violations.Remove(v);
            //}
            //context.SaveChanges();
        }

        /// <summary>
        /// Вставляет одну запись в базу. Должен использовать Unity of Work
        /// чтобы использовать синхронизацию через LockManager
        /// </summary>
        /// <param name="state"></param>
        private void InsertViolationTask(object state)
        {
            TaskState taskState = (TaskState)state;

            Violation entity = new Violation()
            {
                Prefix = taskState.prefix,
                EmployeeFIO = "FIO1",
                ViolationTypeDescription = "ViolationType1",
                Date = DateTime.Now
            };

            // The DbContext is not thread-safe
            //var context = new AspunDbContext();

            
            RepositoryFactories rf = new RepositoryFactories();
            RepositoryProvider repositoryProvider = new RepositoryProvider(rf);
            var uow = new AspunUow(repositoryProvider, taskState.lm);

            //var repository = new ViolationRepository(context, uow);

            uow.Violations.Add(entity);

            //context.SaveChanges();
            uow.Commit();
        }

        //private void RemoveLastViolation()
        //{
        //    // The DbContext is not thread-safe
        //    var context = new AspunDbContext();
        //    var repository = new ViolationRepository(context);

        //    repository.Delete(

            

        //    // Уже сохранено
        //    context.SaveChanges();
        //}
    }
}
